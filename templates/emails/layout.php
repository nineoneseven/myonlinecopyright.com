<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?= $subject ?></title>
<style type="text/css">
<!--
body,td,th, a {font-family: Helvetica, Arial, sans-serif;}
-->
</style>
</head>

<body text="#333333" link="#333333" vlink="#333333" alink="#333333" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#F6F6F6">
<table  background="http://hirethere.com/img/bg_271185.png" bgcolor="#F6F6F6" width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
	<tr>
		<td align="center">
			<img src="http://hirethere.com/img/logos/hire-there-logo-200.png" />
		</td>
	</tr>
	<tr>
		<td>
			<table bgcolor="#EEEEEE" width="600" border="0" align="center" cellpadding="6" cellspacing="0">
				<tr>
					<td>
						<table bgcolor="#FFFFFF" width="100%" border="0" align="center" cellpadding="20" cellspacing="0">

							<tr>
								<td >
									<?= $body ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<p>&nbsp;</p>
			<table width="400" align="center">
				<tr>
					<td align="left">
						<small>
							<font color="#999999">Thanks for using HireThere!</font><br />
							<a href="http://hirethere.com" target="_blank">http://hirethere.com</a>
						</small>
					</td>
					<td align="left">
						<small>
							<font color="#999999">175 Varick St<br />
							New York, NY 10014</font>
						</small>
					</td>
				</tr>
			</table>
			<p>&nbsp;</p>

		</td>
	</tr>
</table>
</body>
</html>