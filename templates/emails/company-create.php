<h1>Thanks for joining HireThere!</h1>

<p>
<font size="5">Your company account has been created!</font> To save time, always use the link below to manage your company info, and the jobs you post.
</p>

<p align="center">
	<font size="5">
		<a href="<?= $companyURL ?>/jobs/new" target="_blank" style="background: #333; color: #FFF; display: inline-block; padding: 20px; text-decoration: none;">
			Post a job
		</a>
	</font>
</p>

