<div class="container-fluid">

<?php if ( $editing ) : ?>
		<h1>Update your account</h1>
<?php else : // editing ?>
		<h1>Create an account in 2 easy steps!</h1>
<?php endif // editing ?>



		<hr />



<?php if ( !$editing ) : ?>
		<p class="lead cms">
			Just tell us a little about yourself, enter your credit card info, and you're registering copyrights before your know it!
		</p>
<?php endif // editing ?>



	  <div class="row-fluid">
	    <div class="span9">
			<form action="" method="post" <?= !$editing ? 'id="accountForm"' : '' ?>>
<?php if ( $errors['form'] ) : ?>
				<div class="alert alert-error lead">
					<?= $errors['form'] ?>
				</div>
<?php endif // form errors ?>
			


<?php if ( !$editing ) : ?>
				<fieldset>
					<legend>How much space do you want to start with?</legend>

					<div class="row-fluid">
						<div class="span12 control-group <?= $errors['quantity'] ? 'error' : '' ?>" id="quantityWrapper">
							<div class="btn-group" data-toggle="buttons-radio">
							<?php $pts = array(1,2,3,5,10,25); foreach ($pts as $i) : ?>
								<label class="btn btn-large <?= ($quantity == $i) || (!$errors['quantity'] && $i == 1) ? 'active' : '' ?> <?= $i == 3 ? 'btn-primary' : '' ?>">
									<input type="radio" name="quantity" value="<?= $i ?>" <?= ($quantity == $i) || (!$errors['quantity'] && $i == 1) ? 'checked="checked"' : '' ?> class="hide" /> <?= $i ?> Gbs
								</label>
							<?php endforeach; ?>
							</div><!-- btn-group -->
							<?php if ( $errors['quantity'] ) : ?><span class="help-block"><?= $errors['quantity'] ?></span><?php endif ?>
							
							<div class="well clearfix" id="quantityExplainer">
								<?php foreach ($pts as $i) : ?>
								<div id="quantity<?= $i ?>" class="explainer <?= ($errors['quantity'] != $i) && (!$errors['quantity'] && $i != 1) ? 'hide' : '' ?>" class="row-fluid">
									<div class="span8 <?= $i == 3 ? 'popular' : '' ?>">
										<p class="lead"><?= $i ?> Gb can hold up to:</p>
										<ul class="unstyled">
											<li><?= $i*1000 ?> documents or</li>
											<li><?= $i*300 ?> photos or</li>
											<li><?= $i*150 ?> MP3s</li>
										</ul>
									</div><!-- span8 -->
									<div class="span4 label" id="quatityPrice">
										<p><?= 
										#money_format('$%i', $i * $price) 
										${'price'.$i}
										?>
											<small>per year</small>
										</p>
									</div>
								</div><!-- row -->
								<?php endforeach; ?>
							</div>
						</div>
					</div><!-- row -->
				</fieldset>
<?php endif // editing ?>


				
				<fieldset>
					<legend>About you:</legend>

					<div class="row-fluid">
						<p class="span5 control-group <?= $errors['email'] ? 'error' : '' ?>">
							<label>Your email address:<br />
							<input type="text" name="email" placeholder="Email" value="<?= $email ?>" class="span12" />
							<?php if ( $errors['email'] ) : ?><span class="help-block"><?= $errors['email'] ?></span><?php endif ?>
							</label>
						</p>
			
						<p class="span5 control-group <?= $errors['password'] ? 'error' : '' ?>">
							<label>Choose a password:<br />
							<input type="password" name="password" placeholder="Password" class="span12" />
							<?php if ( $errors['password'] ) : ?><span class="help-block"><?= $errors['password'] ?></span><?php endif ?>
							</label>
						</p>
					</div>
			
					<div class="row-fluid">
						<p class="span5 control-group <?= $errors['name'] ? 'error' : '' ?>">
							<label>Your full name:<br />
							<input type="text" name="name" placeholder="Your full name" value="<?= $name ?>" class="span12" />
							<?php if ( $errors['name'] ) : ?><span class="help-block"><?= $errors['name'] ?></span><?php endif ?>
							</label>
						</p>
			
						<p class="span5 control-group date <?= $errors['dateofbirth'] ? 'error' : '' ?>" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
							Your date of birth:<br />
							<?php
							$orderdate = explode('-', $dateofbirth);
                     		$year  = $orderdate[0];
                     		$month = $orderdate[1];
                     		$day   = $orderdate[2];							
							?>
				    		<select name="day" class="span3">
<?php for ($i = 1; $i <= 31; $i++) : ?>
				    			<option value="<?= $i ?>" <?= $i == $day ? 'selected' : '' ?>><?= $i ?></option>
<?php endfor; ?>
				    		</select>

							<select name="month" class="span3">
<?php for ($i = 1; $i <= 12; $i++) : ?>
                				<option value="<?= date('F', mktime(0,0,0,$i, 1, date('Y'))) ?>" <?= $i == $month ? 'selected' : '' ?>><?= date('F', mktime(0,0,0,$i, 1, date('Y'))) ?></option>
<?php endfor; ?>
				    		</select>

				    		<select name="year" class="span6">
							<?php for ($i = date('Y') - 30; $i < date('Y') ; $i++) : ?>
				    			<option value="<?= $i ?>" <?= $i == $year ? 'selected' : '' ?>><?= $i ?></option>
							<?php endfor; ?>
				    		</select>

							<?php if ( $errors['dateofbirth'] ) : ?><span class="help-block"><?= $errors['dateofbirth'] ?></span><?php endif ?>
						</p>
					</div>

					<div class="row-fluid">
			<?php /*
						<p class="span5 control-group <?= $errors['cityofbirth'] ? 'error' : '' ?>">
							<label>City you were born in:<br />
							<input type="text" name="cityofbirth" placeholder="City you were born in" value="<?= $cityofbirth ?>" />
							<?php if ( $errors['cityofbirth'] ) : ?><span class="help-block"><?= $errors['cityofbirth'] ?></span><?php endif ?>
							</label>
						</p>
			*/ ?>
						<p class="span5 control-group <?= $errors['countryofbirth'] ? 'error' : '' ?>">
							<label>Country you were born in:<br />
							<input type="text" name="countryofbirth" placeholder="Country you were born in" value="<?= $countryofbirth ?>" class="span12" />
							<?php if ( $errors['countryofbirth'] ) : ?><span class="help-block"><?= $errors['countryofbirth'] ?></span><?php endif ?>
							</label>
						</p>

					</div>
				</fieldset>
		
		<?php if ( !$editing ) : ?>
				<div class="row-fluid">
					<p class="control-group span12">
						<label class="string optional">
							<input type="checkbox" name="rememberme" value="<?php 60 * 24 * 14 // minutes ?>" />
							Remember me for two weeks
						</label>
					</p>
				</div>
				
				<fieldset id="fieldset-cc">
					<legend>Your payment info:</legend>

					<div class="row-fluid">
						<p class="span6 control-group <?= $errors['cc_number'] ? 'error' : '' ?>">
							<label>Card Number:<br />
								<input type="text" size="20" maxlength="16" data-stripe="number" placeholder="4242424242424242" value="" class="span12" />
								<?php if ( $errors['cc_number'] ) : ?><span class="help-block"><?= $errors['cc_number'] ?></span><?php endif ?>
							</label>
						</p>
						
						<p class="span2 control-group <?= $errors['cc_cvc'] ? 'error' : '' ?>">
							<label>CVC Code:<br />
								<input type="text" size="3" maxlength="4" data-stripe="cvc" placeholder="424" value="" class="span12" />
								<?php if ( $errors['cc_cvc'] ) : ?><span class="help-block"><?= $errors['cc_cvc'] ?></span><?php endif ?>
							</label>
						</p>
					</div>
					
					<div class="row-fluid">
						<p class="span3 control-group">
							<label>Expiration:<br />
								<input type="text" size="2" maxlength="2" data-stripe="exp-month" placeholder="06" name="cc_month" value="<?= $cc_month ?>" class="span12" />
							</label>
						</p>
						
						<p class="span3 control-group">
							<label><br />
								<input type="text" size="4" maxlength="4" data-stripe="exp-year" placeholder="2016" name="cc_year" value="<?= $cc_year ?>" class="span12" />
							</label>
						</p>

						<p class="span3 control-group <?= $errors['coupon'] ? 'error' : '' ?>">
							<label>Have a discount code?<br />
								<input type="text" size="10" data-stripe="coupon" name="coupon" value="<?= $coupon ?>" class="span12" />
								<?php if ( $errors['coupon'] ) : ?><span class="help-block"><?= $errors['coupon'] ?></span><?php endif ?>
							</label>
						</p>
					</div>
				</fieldset>


				<hr />


				<fieldset>
					<div class="row-fluid">
						<p class="span12 control-group <?= $errors['tos'] ? 'error' : '' ?>">
							<label class="checkbox">
								<input type="checkbox" name="tos" value="1" /> I Agree With The <a href="/terms-service" target="_blank">Terms And Conditions</a>
								<?php if ( $errors['tos'] ) : ?><span class="help-block"><?= $errors['tos'] ?></span><?php endif ?>
							</label>
						</p>
					</div>
				</fieldset>

		<?php endif // editing ?>
		
		
		
		
				<div class="row-fluid">
					<p class="span12">
						<button class="btn btn-primary btn-large" type="submit" data-loading-text="Loading...">
		<?php if ( $editing ) : ?>
							Save your new info
		<?php else : // editing ?>
							Sign up!
		<?php endif // editing ?>
						</button>
					</p>
				</div><!-- row-fluid -->
			</form>
		</div><!-- span -->



	    <div class="span3 sidebar">
<?php if ( $editing ) : ?>
			<div class="liner well text-center" id="accountCurrentQuantity">
				<h4>You currently have:</h4>
				<p class="lead">
					<?= $stripe_subscription_quantity ?>
					<small>Gb</small>
				</p>
				<p>on your account.</p>
<?php /*
				<div class="btn-group" data-toggle="buttons-radio">
					<a href="/account/subscription?quantity=1" class="btn">
						<span class="lead">+</span>
					</a>
					<a href="/account/subscription?quantity=-1" class="btn">
						<span class="lead">-</span>
					</a>
				</div><!-- btn-group -->
*/ ?>
			</div><!-- box -->
<?php else  : // editing ?>	
			<div class="liner cms">
				<h4>Why do you need to know my birthday and where I was born?</h4>
				<p>Copyrights in some countries require this information. We will not share it with anyone, ever, unless you give us permission to.</p>
			</div><!-- box -->
<?php endif // editing ?>
		</div><!-- sidebar -->
		
	</div><!-- row-fluid -->
</div><!-- container-fluid -->



<?php if ( !$editing ) : ?>
	<script type="text/javascript">
	  var stripe_publishable_key = "<?= $stripe_publishable_key ?>";
	</script>
<?php endif // editing ?>