<div class="container-fluid">

	<h1>Sign into your account</h1>

	<hr />

			<form action="" method="post">
	<?php if ( $errors['form'] ) : ?>
				<div class="alert alert-error lead">
					<?= $errors['form'] ?>
				</div>
	<?php endif // form errors ?>
		
				<div class="row-fluid offset1">
					<p class="control-group span4 <?= $errors['email'] ? 'error' : '' ?>">
						<label>Your email address:<br />
						<input type="text" name="email" placeholder="Email" value="<?= $email ?>" />
						<?php if ( $errors['email'] ) : ?><span class="help-block"><?= $errors['email'] ?></span><?php endif ?>
						</label>
					</p>

					<p class="control-group span4 <?= $errors['password'] ? 'error' : '' ?>">
						<label>Your password:<br />
						<input type="password" name="password" placeholder="Password" />
						<?php if ( $errors['password'] ) : ?><span class="help-block"><?= $errors['password'] ?></span><?php endif ?>
						</label>
					</p>
				</div>

				<div class="row-fluid offset1">
					<p class="control-group span4">
						<label class="string optional checkbox">
							<input type="checkbox" name="rememberme" value="<?php 60 * 24 * 14 // minutes ?>" />
							Remember me for two weeks
						</label>
					</p>
				</div>

				<div class="row-fluid offset1">
					<p class="span4">
						<button class="btn btn-primary" type="submit" data-loading-text="Loading...">
							Sign in!
						</button>
					</p>
				</div>

				<div class="row-fluid offset1">
					<p>
						<a href="/account/password">Lost your password?</a>
					</p>
				</div>
			</form>
		</div><!-- row -->


<?php /*
	    <div class="span4 sidebar">
			<div class="liner">
				<h4>Why do you need to know my birthday and where I was born?</h4>
				<p>Copyrights in some countries require this information. We will not share it with anyone, ever, unless you give us permission to.</p>
			</div><!-- box -->
		</div><!-- sidebar -->
*/ ?>
	</div><!-- row-fluid -->

</div><!-- container-fluid -->