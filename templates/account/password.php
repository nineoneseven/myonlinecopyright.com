<div class="container-fluid">

	<h1>Reset your password</h1>

	<hr />

	<p class="lead cms">Enter your account email address below and we'll send you a new password.</p>


	<form action="" method="post" <?= !$editing ? 'id="accountForm"' : '' ?>>
<?php if ( $errors['form'] ) : ?>
				<div class="alert alert-error lead">
					<?= $errors['form'] ?>
				</div>
<?php endif // form errors ?>


				
				<fieldset>
					<div class="row-fluid">
						<p class="span5 control-group <?= $errors['email'] ? 'error' : '' ?>">
							<label>Your email address:<br />
							<input type="text" name="email" placeholder="Email" value="<?= $email ?>" class="span12" />
							<?php if ( $errors['email'] ) : ?><span class="help-block"><?= $errors['email'] ?></span><?php endif ?>
							</label>
						</p>
			
					</div>
				</fieldset>
		
		
		
		
				<div class="row-fluid">
					<p class="span12">
						<button class="btn btn-primary btn-large" type="submit" data-loading-text="Loading...">
							Reset it!
						</button>
					</p>
				</div><!-- row-fluid -->
			</form>
</div><!-- container-fluid -->