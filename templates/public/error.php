<div class="container-fluid">
	<h1>Whoops!</h1>

	<hr />

	<p class="lead">
	<?php if ($message) : ?>
	<?= $message ?>
	<?php else : // message ?>
	Application error.
	<?php endif // message ?>
	</p>
</div>