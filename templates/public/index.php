<div id="myCarousel" class="carousel slide">
	<div class="container-fluid">
		<div class="carousel-inner">
			<div class="item active" id="music">
				<img src="/img/homepage/music.jpg" alt="">
				<div class="carousel-caption color" style="background: #e66125;"></div>
				<div class="carousel-caption cms">
					<h1> <b>Always</b>
						use protection.
					</h1>
					<p class="lead">
						Copyright your song, track, or musical composition.
					</p>
				</div>
			</div><!-- music -->

			<div class="item" id="design">
				<img src="/img/homepage/design.jpg" alt="">
				<div class="carousel-caption color" style="background: #e25378;"></div>
				<div class="carousel-caption cms">
					<h1> <b>Always</b>
						use protection.
					</h1>
					<p class="lead">
						Copyright your drawing, logo, or design.
					</p>
				</div>
			</div><!-- design-->

			<div class="item" id="science">
				<img src="/img/homepage/science.jpg" alt="">
				<div class="carousel-caption color" style="background: #928e18;"></div>
				<div class="carousel-caption cms">
					<h1> <b>Always</b>
						use protection.
					</h1>
					<p class="lead">
						Copyright your formula, algorithm, paper or lab notes.
					</p>
				</div>
			</div><!-- science -->

			<div class="item" id="photo">
				<img src="/img/homepage/photo.jpg" alt="">
				<div class="carousel-caption color" style="background: #e25378;"></div>
				<div class="carousel-caption cms">
					<h1> <b>Always</b>
						use protection.
					</h1>
					<p class="lead">
						Copyright your photo, collage, or original artwork.
					</p>
				</div>
			</div><!-- photo -->

			<div class="item" id="writing">
				<img src="/img/homepage/writing.jpg" alt="">
				<div class="carousel-caption color" style="background: #ba6488;"></div>
				<div class="carousel-caption cms">
					<h1> <b>Always</b>
						use protection.
					</h1>
					<p class="lead">
						Copyright your poem, novel, short story or creative writing.
					</p>
				</div>
			</div><!-- writing -->

		</div><!-- carousel-inner -->

		<a class="left carousel-control" href="carousel.html#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="right carousel-control" href="carousel.html#myCarousel" data-slide="next">&rsaquo;</a>
	</div><!-- container -->
</div>
<!-- /.carousel -->
<div class="container-fluid" id="sectionIntro">
	<div class="row-fluid">
		<div class="span6" id="logoCol">
			<h1>
				<img src="/img/logos/moc-vert-146x175.png" />
				My Online
				<br />
				Copyright
			</h1>
		</div>
		<!-- span6 -->
		<div class="span6 cms">
			<p class="lead">
				You sweat over your creations - whether it's art or invention. 
						Be sure <b>you</b> get the fame, the millions, the <b>recognition</b> for your original idea. 
						With MyOnlineCopyright, it takes just 20 seconds to <b>protect your genius</b>.
			</p>
			<p>
				<a href="/account/register" class="btn btn-large btn-primary">Sign up now!</a>
			</p>
		</div>
		<!-- span6 -->
	</div>
	<!-- row -->
</div>
<!-- container-fluid -->
<div id="sectionBoxes">
	<div class="container-fluid">
		<div class="row-fluid top">
			<div class="span4 left cms" id="boxMusic">
				<p>
					Rap, jazz or classical, your beats should stay yours. Protect them now.
				</p>
				<h4> <i class="icon"></i>
					Music
				</h4>
			</div>
			<!-- span4 -->
			<div class="span4 center cms" id="boxScience">
				<p>
					You've created the next Google algorithm. Make sure that you get the millions you deserve.
				</p>
				<h4> <i class="icon"></i>
					Science
				</h4>
			</div>
			<!-- span4 -->
			<div class="span4 right cms" id="boxDesign">
				<p>
					Whether your pushing pencils or pixels, we'll protect your original designs.
				</p>
				<h4>
					<i class="icon"></i>
					Design
				</h4>
			</div>
			<!-- span4 -->
		</div>
		<!-- row -->
		<div class="row-fluid bottom">
			<div class="span4 left cms" id="boxMedia">
				<p>
					Film, tv, video or multimedia, every frame it's your frame. Copyright them today.
				</p>
				<h4>
					<i class="icon"></i>
					Media
				</h4>
			</div>
			<!-- span4 -->
			<div class="span4 center cms" id="boxWriting">
				<p>
					If you write scripts, book, novels, poems, every word tells a story, yours.
				</p>
				<h4>
					<i class="icon"></i>
					Writing
				</h4>
			</div>
			<!-- span4 -->
			<div class="span4 right cms" id="boxIdeas">
				<p>
					Great ideas come and go. Unleash your genius, but protect it first.
				</p>
				<h4>
					<i class="icon"></i>
					Ideas
				</h4>
			</div>
			<!-- span4 -->
		</div>
		<!-- row -->
	</div>
	<!-- container-fluid -->
</div>
<div id="sectionPricing">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span8 cms">
				<h1>
					<b>Always</b>
					use protection.
				</h1>
				<p>
					For a single price, once a year, you get 1Gb of secure cloud space (or more if you need it!) plus all the bells and whistles we offer. 
					You can upload and protect as many original files as you want. We keep them safe and secure in the cloud for you, for as long as you need. 
					Upload Hundreds of original songs, thousands of pictures and sketches, hundreds of thousand of pages or text, and so on.
				</p>
				<p>
					<a href="/account/register" class="btn btn-large btn-primary">Sign up now!</a>
				</p>
			</div>
			<!-- span8 -->
			<div class="span4" id="sectionPricingBubble">
				<p>
					<span class="lead">$<?= $price ?></span>
					per year
				</p>
			</div>
			<!-- span8 -->
		</div>
		<!-- row -->
	</div>
</div>
<!-- sectionPricing -->