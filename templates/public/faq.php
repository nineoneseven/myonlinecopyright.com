<style>
dt {margin: 4em 0 1em;}
</style>

<div class="container-fluid">

	<h1>Frequently Asked Questions</h1>

	<hr />

	<dl>
		<dt>
			How does the MyOnlineCopyright.com process work?
		</dt>
		<dd>
			<p>MyOnlineCopyright.com provides a third-party, registered dating of your original digital work. By using this service, you publicly associate your digital work to you beginning from a specific date.  The date-registering of your original work may be helpful to you in asserting a copyright in that work.  However, this site does not guarantee that you have a copyright of your work.</p>
		</dd>

		<dt>
			So, how does MyOnlineCopyright.com date register my original work?
		</dt>
		<dd>
			<p>
				Every digital file has a unique makeup of bits and bytes which is its “Digital DNA”. MyOnlineCopyright.com captures your original creation's “Digital DNA”, stores the “Digital DNA” in a database and sends a copy of the “Digital DNA” to you in an email. The email contains the verified date; the “Digital DNA” verifies the digital creation, and your email address verifies it belongs to you.
			</p>

			<p>
				NOTE: You must keep the email with this “Digital DNA”. This email is your date registered proof for your creation.
			</p>

			<p>
				MyOnlineCopyright.com stores the “Digital DNA” as well, which allows you and others to return to this service and verify the date of creation of your work.
			</p>
		</dd>

		<dt>Why do I need to date register my original work?</dt>
		<dd>
			<p>
				It is important to date register your original work with a third party at the soonest possible point. Registering creates a public historical record associating your original work to you, which may be helpful to you in asserting a copyright in that work.
			</p>
		</dd>

		<dt>How can the registered date of my creation be verified?</dt>
		<dd>
			<p>
				There is a link, “verify registration date”, on the bottom navigation of this site which allows anyone to find a the registration date of a work by submitting the original file, or entering the “MOC REGISTRATION CODE”, on the website.
			</p>
		</dd>

		<dt>To what types of creations may copyrights attach?</dt>
		<dd>
			<p>
				What you can copyright is viewed broadly and includes almost all literary, musical, dramatical, graphical, audiovisual, sound, architectural works.
			</p>

			<p>
				The United States Copyright Office’s website (www.copyright.gov) states, “Copyright protects original works of authorship that are fixed in a tangible form of expression. The fixation need not be directly perceptible so long as it may be communicated with the aid of a machine or device.”
			</p>
			<p>
				Please consult with an attorney as to whether you hold a copyright in your work.  This service only provides proof of the date of creation that may helpful to you in asserting a copyright in your work.  This service does not guarantee that you have a copyright in your work.
			</p>
		</dd>

		<dt>What is a “Digital DNA”?</dt>
		<dd>
			<p>
				A “Digital DNA”, is a hash value created by a hash algorithm. The 256-bit Secure Hash Algorithm is used by MyOnlineCopyright.com and is an industry standard way of creating a “Digital DNA” of a file. The algorithm mixes and chops the data of a file to create a unique string of numbers and letters based on the files makeup.
			</p>
		</dd>

		<dt>What do the numbers and letters in the “Digital DNA” mean?</dt>
		<dd>
			<p>
				The letters and numbers in a “Digital DNA” are created by the algorithm MyOnlineCopyright.com uses to determine the uniqueness of your original digital creation. This combination of letters and numbers will always be unique to your digital creation as long as the creation is not changed.
			</p>
		</dd>

		<dt>
			What if I change the digital creation that I had previously date registered?
		</dt>
		<dd>
			<p>
				Then you will need to establish another date registration for your original digital creation to reflect its current and changed state, uploading the new file to the MyOnlineCopyright.com platform.
			</p>
		</dd>

		<dt>Can I date-register my original work under an alias?</dt>
		<dd>
			<p>
				No. You must provide your legal name, birthdate, and country to MyOnlineCopyright.com.  The date registration will likely not be useful to you if the work is registered under an alias.
			</p>
		</dd>

		<dt>Is you website secure?</dt>
		<dd>
			<p>
				Yes. We use SSL technologies, so your connection to myonlinecopyright.com is encrypted with 256-bit encryption.
			</p>
			<p>
				The connection is encrypted using AES_256_CBC, with SHA1 for message authentication and DHE_RSA as the key exchange mechanism.
			</p>
		</dd>
	</dl>
</div>
<!-- container-fluid -->