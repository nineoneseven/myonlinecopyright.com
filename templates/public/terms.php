<div class="container-fluid cms">
	<h1>￼Terms and Conditions</h1>

	<hr />

	<p>All the services and information available on all of the
	MyOnlineCopyright.com websites are provided to you by MYONLINECOPYRIGHT LLC. By
	accessing any of the MyOnlineCopyright.com, pages or using any services
	provided, you are agreeing to understand and be linked to the terms and
	conditions (“Terms of Service”) and Privacy Policy.</p>
	<p>MyOnlineCopyright.com reserves the right to change or update the
	Terms of Service at any time without notice. All new features and
	services will be included under the MyOnlineCopyright.com Terms of
	Service and with your use of our services and site, you acknowledge and
	agree to these “Terms of Service”.</p>
	<h3>Account Terms</h3>
	<p>You must be the original creator of the digital creation you are date
	registering with MyOnlineCopyright.com.</p>
	<p>You must be human. Any accounts shown to be created by “bots” or
	other automated methods are not allowed.</p>
	<p>You must provide your legal personal information when required during
	the course of using this site’s services.</p>
	<p>All accounts created are for the use of only one person. Shared
	accounts are not permitted and are subject to termination and deletion
	of all account data.</p>
	<p>You are responsible for your account passwords. MyOnlineCopyright.com
	is not responsible for any data loss or damage to your account
	information if these passwords are lost or forgotten.</p>
	<p>You are responsible for content posted to your account.</p>
	<p>One person or legal entity is allowed only one free account. Abuse of
	this will result in all accounts held by the individual being terminated
	and the data deleted.</p>
	<p>You many not use this service for any illegal or unauthorized
	purposes. You must not violate any laws in your jurisdiction</p>
	<p>Any violation of these agreements will result in account termination
	and immediate deletion of all content contained within that account. By
	using the services provided by MyOnlineCopyright.com, you claim all
	responsibility for enforcing your copyright and understand that
	MyOnlineCopyright.com is not responsible for any misunderstandings of
	legal requirements in your jurisdiction for the enforcement of your
	copyright.</p>
	<h3>Modification of Terms</h3>
	<p>MyOnlineCopyright.com reserves the right at any time to modify any
	part of the Terms of Service with or without notice.</p>
	<p>MyOnlineCopyright.com reserves the right to change the services
	provided, prices charged and features offered with a 30-day notice. This
	notice will be placed on the site or sent to all accounts as an email.</p>
	<h3>Cancellation</h3>
	<p>You are responsible for cancelling your account using the settings
	page accessed once you have logged into the site. No other forms of
	cancellation will be accepted.</p>
	<p>Upon cancellation all content and data contained in the account will
	be permanently deleted.</p>
	<p>If you cancel an account, the cancellation will take place
	immediately and you will not be charged going forward.</p>
	<p>MyOnlineCopyright.com reserves the right to cancel or freeze your
	account at any time for any reason.</p>
	<p>MyOnlineCopyright.com reserves the right to deny an account to anyone
	at any time.</p>
	<h3>Infringement</h3>
	<p>It is your responsibility to ensure that any digital creation you
	date register with MyOnlineCopyright.com is your original creation and
	has not violated any Copyright law in your jurisdiction.</p>
	<p>MyOnlineCopyright.com reserves the right to remove a register for a
	copyright at any time for any reason.</p>
	<h3>General Conditions</h3>
	<p>Your use of this service is at your own risk. The service provided is
	on an as is and as available basis.</p>
	<p>Technical support is available to paying customers and is available
	by email only. MyOnlineCopyright.com will respond in as timely a manner
	as it can.</p>
	<p>You cannot hack, modify or falsely claim any of the
	MyOnlineCopyright.com site or services.</p>
	<p>You understand that some aspects of the MyOnlineCopyright.com
	service, including your content, may be transmitted over unencrypted
	networks and transmitted to various machines over the network.</p>
	<p>If your bandwidth usage exceeds the average bandwidth usage
	determined by MyOnlineCopyright.com, your account is subject to a
	bandwidth throttle and may be disabled.</p>
	<p>MyOnlineCopyright.com does not guarantee that the services provided
	will meet your need; as the consumer of these services, it’s your
	responsibility to ensure that the services do meet your needs in your
	jurisdiction.</p>
	<p>You expressly understand and agree that MyOnlineCopyright.com shall
	not be liable for any direct, indirect, or incidental damage, including
	but not limited to, your content, data, or goodwill.</p>
	<p>The failure of MyOnlineCopyright.com to enforce the Terms of Service
	does not waiver any rights or provisions. This includes prior Terms of
	Service.</p>
</div>