<div class="container-fluid">

	<h1>Contact us</h1>

	<hr />

	<div class="row-fluid">
		<div class="span8">
	<?php if ( $isSent ) : ?>
			<div class="alert alert-success lead">
			  Great, your email has been sent. I'll get back to you as soon as I can!
			</div>
	<?php else : // isSent ?>
			<form action="" method="post">
	<?php if ( $errors ) : ?>
				<div class="alert alert-error lead">
	<?php if ( $errors['form'] ) : ?>
		<?= $errors['form'] ?>
	<?php else : ?>
		Please check the errors below.
	<?php endif ?>
				</div>
	<?php endif // errors ?>


				<p class="control-group <?= $errors['email'] ? 'error' : '' ?>">
					<label>Your email address<br />
						<input type="text" name="email" class="input-xxlarge" placeholder="e.g. you@home.com" value="<?= $email ?>" />
						<?php if ( $errors['email'] ) : ?><span class="help-block"><?= $errors['email'] ?></span><?php endif ?>
					</label>
				</p>
				
				<p class="control-group <?= $errors['message'] ? 'error' : '' ?>">
					<label>Your message<br />
						<textarea name="message" class="input-xxlarge" rows="10"><?= $message ?></textarea>
						<?php if ( $errors['message'] ) : ?><span class="help-block"><?= $errors['message'] ?></span><?php endif ?>
					</label>
				</p>
				
				<hr /> 
	 
				<p>
					<button class="btn-large btn-inverse" type="submit" data-loading-text="Loading...">Email away!</button>
				</p>

			</form>
	<?php endif // isSent ?>
		</div>
		
		
		
		<div class="pull-right span3">
		</div>
	</div>
</div><!-- container-fluid -->