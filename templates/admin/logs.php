<style>
table {padding: 0; width: 100%;}
textarea {height: 20em; width: 96%;}
</style>

<div class="container-fluid">

	<h1>Admin</h1>

	<hr />

	<table border="1">
		<tr>
			<td width="33%" valign="top">
				<ul>
	<?php foreach ($filesArr as $file) : ?>
					<li>
						<a href="<?= $path ?>?file=<?= urlencode($file) ?>"><?= $file ?></a>
					</li>
	<?php endforeach ?>
				</ul>
			</td>
			<td valign="top">
				<textarea disabled="true"><?= $content ?></textarea>
			</td>
		</tr>
	</table>
</div><!-- container-fluid -->