
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Online Copyright</title>

<?php if ( $pageId == 'page-public-index' ) : // homepage ?>
<meta name="description" content="Be sure you get the fame, the millions, the recognition for your original idea. Take just 20 seconds and protect your genius." /> 

<meta property="og:type" content="website" />
<meta property="og:image" content="http://myonlinecopyright.com/img/logos/moc-logo-sq-500.png" />
<meta property="og:title" content="Always use protection. Take just 20 seconds and protect your original ideas." />
<meta property="og:description" content="Be sure you get the fame, the millions, the recognition for your original idea. Take just 20 seconds and protect your genius." />
<meta property="og:url" content="https://myonlinecopyright.com" />

<?php endif ?>

<?php if ( APPLICATION_ENV == 'production' ) : ?>
<script type="text/javascript">
	var analytics=analytics||[];(function(){var e=["identify","track","trackLink","trackForm","trackClick","trackSubmit","page","pageview","ab","alias","ready","group"],t=function(e){return function(){analytics.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var n=0;n<e.length;n++)analytics[e[n]]=t(e[n])})(),analytics.load=function(e){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"===document.location.protocol?"https://":"http://")+"d2dq2ahtl5zl1z.cloudfront.net/analytics.js/v1/"+e+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n)};
	analytics.load("ck43a30hmw");

<?php if ( $signedIn ) : ?>
	analytics.identify('<?= $account->id ?>', {
		email : '<?= $account->email ?>'
	});
<?php endif; // $signedIn ?>
</script>
<?php endif // production ?>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->


<!-- Le styles -->
<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->

<link href="/css/common.css" rel="stylesheet">
<?= $css ?>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="/bootstrap/js/html5shiv.js"></script>
    <![endif]-->

</head>

<body class="<?= $pageClassList ?> <?= $signedIn ? 'signedIn' : '' ?>" id="<?= $pageId ?>">
<div id="wrapper">



<?php if ($isAdmin) : ?>
	<div class="navbar " id="navbarAdmin">
		<div class="container-fluid"><!-- Collapsable nav bar --> 
			Signed in as <a href="/admin">admin</a> |
			<a href="/admin">Admin home</a> |
			<a href="/admin/logs">Logs</a>

		</div>

	</div>
<?php endif // admin ?>



	<div class="navbar xxnavbar-fixed-top" id="navbarTop">
<?php /*		<div class="navbar-inner"> */ ?>
			<div class="container-fluid"><!-- Collapsable nav bar --> 
			
<?php /*
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
				</a> 
*/ ?>
	
	
				<a class="brand" href="/">
					<img src="/img/logos/moc-horz-69x57.png" /> 
					My Online Copyright
				</a>
				
				
	
<?php /*				<div class="nav-collapse"> */ ?>
					<!-- http://mifsud.me/adding-dropdown-login-form-bootstraps-navbar/ -->
					<ul class="nav pull-right" id="navAccount">
						<li class="dropdown public signin">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown">
								Sign In 
<!--								<strong class="caret"></strong>-->
							</a>
	
							<div class="dropdown-menu dropdown-form" id="signinForm">
								<form action="/signin" method="post">
									<p class="control-group"><input type="text" name="email" placeholder="Email" /></p>
									<p class="control-group"><input type="password" name="password" placeholder="Password" /></p>
									<p class="control-group"><label class="string optional"> 
										<input type="checkbox" name="user[remember_me]" value="1" /> Remember me </label>
									</p>
								
									<p>
										<button class="btn btn-primary" type="submit" data-loading-text="Loading...">Sign in!</button>
									</p>
									<p>
										<a href="/account/password">Lost your password?</a>
									</p>
								</form>
							</div>
						</li>
						<li class="public register">
							<a href="/account/register">
								<span class="css-arrow"></span>
								Register
							</a>
						</li>
					</ul>
					
					
					
					<ul class="nav pull-right" id="navPages">
						<li class="app"><a href="/app">Your copyrights</a></li>
						<li class="divider app">/</li>
						<li class="app"><a href="/account">Your account</a></li>
						<li class="public faq"><a href="/frequently-asked-questions">FAQ</a></li>
						<li class="divider">/</li>
						<li class="public contact"><a href="/contact-us">Contact us</a></li>
						<li class="app"><a href="/account/signout">Sign out</a></li>
					</ul>
					


<?php /*				</div><!-- nav-collapse --> */ ?>
			</div><!-- container -->
<?php /*		</div><!-- navbar-inner --> */ ?>
	</div><!-- navbar -->



	<div id="bodyWrapper">

<?php if ( count(Model_Flash::$messages) > 0 ) : foreach( Model_Flash::$messages as $id => $msg ) : ?>
		<div class="container-fluid" id="alertsWrapper">
			<div class="alert alert-<?= $id ?> lead">
				<?= $msg ?>
			</div>
		</div>
<?php endforeach; endif ?>



		<?= $body ?>
	</div><!-- bodyWrapper --> 



	<footer>
		<div class="container-fluid">
			<div class="row-fluid text-center">
				<ul class="nav nav-pills">
					<li><a href="/contact-us">Contact us</a></li>
					<li><a href="/copyright/verification">Verify a copyright</a></li>
					<li><a href="/frequently-asked-questions">frequently asked questions</a></li>
					<li><a href="/privacy-policy">Privacy policy</a></li>
					<li><a href="/terms-service">Terms and conditions</a></li>
				</ul>
			</div><!-- row -->
			
			<div class="row-fluid">
				<p class="text-center">&copy; MyOnlineCopyright, <?= date('Y') ?></p>
			</div><!-- row -->
		</div><!-- container -->
	</footer>

</div><!-- wrapper -->



<script src="/js/jquery-1.9.1.min.js"></script>
<script src="/bootstrap/js/bootstrap-transition.js"></script>
<!--<script src="/bootstrap/js/bootstrap-alert.js"></script>-->
<script src="/bootstrap/js/bootstrap-modal.js"></script>
<script src="/bootstrap/js/bootstrap-dropdown.js"></script>
<!--<script src="/bootstrap/js/bootstrap-scrollspy.js"></script>-->
<!--<script src="/bootstrap/js/bootstrap-tab.js"></script>-->
<script src="/bootstrap/js/bootstrap-tooltip.js"></script>
<!--<script src="/bootstrap/js/bootstrap-popover.js"></script>-->
<script src="/bootstrap/js/bootstrap-button.js"></script>
<!--<script src="/bootstrap/js/bootstrap-collapse.js"></script>-->
<script src="/bootstrap/js/bootstrap-carousel.js"></script>
<!--<script src="/bootstrap/js/bootstrap-typeahead.js"></script>-->

<script src="/js/common.js"></script>
<?= $js ?>

<script>
var ACCOUNT_TOKEN = '<?= $accountToken ?>';
</script>


<?php if ($isAdmin) : ?>
	<script src="/js/cms.js"></script>

<div id="cmsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cmsModalLabel" aria-hidden="true">
	<form action="/admin/cms" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Content editor</h3>
		</div>
		<div class="modal-body">
			<p>
				<textarea name="new"></textarea>
				<input type="hidden" name="old" value="" />
				<input type="hidden" name="template" value="<?=	$template ?>" />
				<input type="hidden" name="referer" value="<?= (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://") . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] ?>" />
			</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>

<?php endif // admin ?>

</body>
</html>
