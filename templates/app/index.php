<div class="container-fluid">

<form id="fileupload" action="" method="POST" enctype="multipart/form-data">

	<div class="row-fluid fileupload-buttonbar">
		<div class="span6">
			<h1>Your Copyrights</h1>
		</div>

		<div class="span2 pull-right">
			<span class="btn btn-success fileinput-button">
				<i class="icon-plus icon-white"></i>
				Add files...
				<input type="file" name="files[]" multiple>
			</span>
		</div><!-- span6 -->
	</div><!-- row -->



	<div class="row-fluid" id="infoPanel">
		<div class="span4">
			Copyrights: <?= $account->files_count ?>
		</div>

		<div class="span4">
			Diskspace used: <?= $account->files_diskspaceused ?>
		</div>

		<div class="span4">
			Remaining diskspace: <?= $files_diskspaceremaining ?>
		</div>
	</div>


	<div class="row-fluid">
		<div class="span12 fileupload-progress fade">
			<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				<div class="bar" style="width:0%;"></div>
			</div>
			<div class="progress-extended">&nbsp;</div>
		</div><!-- span12 -->
	</div><!-- row -->



	<table class="table hide" id="filesTable">
		<thead>
			<tr>
				<th>Name</th>
				<th>description</th>
				<th>Size</th>
				<th>Type</th>
				<th>Created</th>
			</tr>
		</thead>
		<tbody class="files"></tbody>
	</table>



	<div class="well lead" id="noFilesWell">
		<img src="/img/loading-circle.gif" />
		Loading your files 
	</div>
	
</form>



<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload">
        <td>
			{%=o.formatLongText(file.name, {length: 20})%}
		</td>
        {% if (file.error) { %}
            <td class="error" colspan="3"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td colspan="3">
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
	        <td>{%=o.formatFileSize(file.size)%}</td>
        {% } %}
    </tr>
{% } %}
</script>



<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}

{% if (file.ismetadatacomplete) { %}
    <tr class="template-download">
{% } else { %}<!-- ismetadatacomplete -->
    <tr class="template-download incomplete">
{% } %}<!-- ismetadatacomplete -->

{% if (file.error) { %}
            <td>
				<i class="icon-{%= file.icon %}"></i>
				{%= file.filenameShort %}
			</td>
            <td>{%= file.filesizeFormated %}</td>
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
{% } else { %}
            <td>
				<a href="/copyrights/{%= file.id %}" class="edit">
					<i class="icon-{%= file.icon %}"></i>
					{%= file.filenameShort %}
				</a>
            </td>
{% if (file.ismetadatacomplete) { %}
			<td>
{% if (file.description) { %}
				{%= file.descriptionShort %}
{% } %}<!-- description -->
				<a href="/copyrights/{%= file.id %}/edit">edit</a>
			</td>
            <td>{%= file.filesizeFormated %}</td>
            <td>{%= file.simpleFiletype %}</td>
			<td>{%= file.createdFriendly %}</td>
{% } else { %}<!-- ismetadatacomplete -->
			<td colspan="4">
				<a href="/copyrights/{%= file.id %}">
					Click here to finish registering this copyright.
				</a>
			</td>
{% } %}<!-- ismetadatacomplete -->
{% } %}<!-- error -->
    </tr>
{% } %}
</script>


</div><!-- container-fluid -->