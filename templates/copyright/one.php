<div class="container-fluid">
	<h1><?= $copyright->filename ?></h1>

	<hr />

	<form action="" method="post">

<?php if ( $copyright->ismetadatacomplete ) : ?>
		<h3>Copyrighted: <?= $copyright->createdFriendly ?></h3>

		<div class="row-fluid">
			<div class="span6">
				<a href="/copyrights/<?= $copyright->id ?>/certificate" target="_blank" class="certificate btn"><i class="icon-flag"></i>Download your certificate</a>
				<a href="/copyrights/<?= $copyright->id ?>/download" class="download btn"><i class="icon-arrow-down"></i>Download the file</a>
			</div>

			<div class="span6 text-right">
<?php if ( $editing ) : ?>
				<a href="/copyrights/<?= $copyright->id ?>/delete" class="btn"><i class="icon-trash"></i> Delete</a>
<?php else : // editing ?>
				<a href="/copyrights/<?= $copyright->id ?>/edit" class="btn">Edit</a>
<?php endif // editing ?>
			</div>
		</div>

		<hr />
<?php endif ?>

		<div class="row-fluid">
			<div class="span2">
				<h4>Type:</h4>
				<p>
				<i class="icon-<?= $copyright->icon ?>"></i>
				<?= $copyright->simpleFiletype ?>
				</p>
			</div>

			<div class="span2">
				<h4>Size:</h4>
				<p><?= $copyright->filesizeFormated ?></p>
			</div>

<?php if ( $copyright->ismetadatacomplete ) : ?>
			<div class="span3">
				<h4>License:</h4>
				<p><?= $copyright->license ?></p>
			</div>
<?php endif ?>
		</div><!-- row -->

		<hr />

		<div class="row-fluid">
			<div class="control-group span2 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
				<h4>
					Published: 
					<i class="icon-question-sign" data-toggle="tooltip" title="Has your work been published yet?"></i>
				</h4>
<?php if ( $editing ) : ?>
				<div class="btn-group" data-toggle="buttons-radio">
					<label class="btn <?= $copyright->ispublished && $copyright->ismetadatacomplete ? 'active' : '' ?>">
						<input type="radio" name="ispublished" value="1" <?= $copyright->ispublished && $copyright->ismetadatacomplete ? 'checked="checked"' : '' ?> class="hide" /> Yes
					</label>

					<label class="btn <?= !$copyright->ispublished && $copyright->ismetadatacomplete ? 'active' : '' ?>">
						<input type="radio" name="ispublished" value="0" <?= !$copyright->ispublished && $copyright->ismetadatacomplete ? 'checked="checked"' : '' ?> class="hide" /> No
					</label>
				</div><!-- btn-group -->
<?php else : // editing ?>
				<?= $copyright->ispublished ? 'Yes' : 'No' ?>
<?php endif // editing ?>
			</div>



			<div class="<?= (bool) $copyright->ispublished != TRUE ? 'hide' : '' ?> pub-toggle control-group span4 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
				<h4>
					Publication date 
<?php if ( $editing ) : ?>
					<small>(optional)</small>
<?php endif // editing ?>
					:
					<i class="icon-question-sign" data-toggle="tooltip" title="If it's been published, when was it published?"></i>
				</h4>
				<p>
<?php if ( $editing ) : ?>
					<select name="day" class="span3">
<?php for ($i = 1; $i <= 31; $i++) : ?>
		    			<option value="<?= $i ?>" <?= $i == $day ? 'selected' : '' ?>><?= $i ?></option>
<?php endfor; ?>
		    		</select>

					<select name="month" class="span3">
<?php for ($i = 1; $i <= 12; $i++) : ?>
        				<option value="<?= date('F', mktime(0,0,0,$i, 1, date('Y'))) ?>" <?= $i == $month ? 'selected' : '' ?>><?= date('F', mktime(0,0,0,$i, 1, date('Y'))) ?></option>
<?php endfor; ?>
		    		</select>

		    		<select name="year" class="span6">
<?php for ($i = date('Y') - 100; $i <= date('Y') ; $i++) : ?>
    					<option value="<?= $i ?>" <?= $i == date('Y') - 30 || $i == $year ? 'selected' : '' ?>><?= $i ?></option>
<?php endfor; ?>
		    		</select>
<?php else : // editing ?>
					<?= substr($copyright->publisheddate, 0, 1) != 0 ? date('j F, Y', strtotime($copyright->publisheddate)) : '<small>(not published)</small>' ?>
<?php endif // editing ?>
				</p>
			</div>


<?php /*
			<div class="<?= $copyright->ispublished !== '0' && $editing ? 'hide' : '' ?> pub-toggle control-group span2 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
				<h4>
					Original:
					<i class="icon-question-sign" data-toggle="tooltip" title="Is your work original, or a derivative of an existing copyrighted work?"></i>
				</h4>
<?php if ( $editing ) : ?>
				<div class="btn-group" data-toggle="buttons-radio">
					<label class="btn <?= $copyright->isoriginal && $copyright->ismetadatacomplete ? 'active' : '' ?>">
						<input type="radio" name="isoriginal" value="1" <?= $copyright->isoriginal && $copyright->ismetadatacomplete ? 'checked="checked"' : '' ?> class="hide" /> Yes
					</label>

					<label class="btn <?= !$copyright->isoriginal && $copyright->ismetadatacomplete ? 'active' : '' ?>">
						<input type="radio" name="isoriginal" value="0" <?= !$copyright->isoriginal && $copyright->ismetadatacomplete ? 'checked="checked"' : '' ?> class="hide" /> No
					</label>
				</div><!-- btn-group -->
<?php else : // editing ?>
				<?= $copyright->isoriginal ? 'Yes' : 'No' ?>
<?php endif // editing ?>
			</div>
*/ ?>


			<div class="<?= $copyright->ispublished != TRUE ? 'hide' : '' ?> pub-toggle control-group span4 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
				<h4>
					Pseudonym 
<?php if ( $editing ) : ?>
					<small>(optional)</small>
<?php endif // editing ?>
					:
					<i class="icon-question-sign" data-toggle="tooltip" title="Did you publish it using a pseudonym?"></i>
				</h4>
				<p>
<?php if ( $editing ) : ?>
					<input type="text" name="pseudonym" value="<?= $copyright->pseudonym ?>" />
<?php else : // editing ?>
					<?= $copyright->pseudonym ? $copyright->pseudonym : '<small>(none)</small>' ?>
<?php endif // editing ?>
				</p>
			</div>
		</div><!-- row -->

		<hr />

		<div class="row-fluid">
			<div class="span12">
				<h4>
					Description 
<?php if ( $editing ) : ?>
					<small>(optional)</small>
<?php endif // editing ?>
					:
				</h4>
<?php if ( $editing ) : ?>
				<p>
					<textarea rows="2" class="input-xxlarge" name="description"><?= $copyright->description ?></textarea>
				</p>
				<p>
					<button type="submit" class="btn btn-primary" data-loading-text="Loading...">Save</button>
<?php if ( $copyright->ismetadatacomplete ) : ?>
					<a href="/copyrights/<?= $copyright->id ?>" class="btn btn-secondary">Cancel</a>
<?php endif // ismetadatacomplete ?>
				</p>
<?php else : // editing ?>
				<p>
					<?= $copyright->description ?> <a href="/copyrights/<?= $copyright->id ?>/edit" class="editDescription">edit</a>
				</p>
<?php endif // editing ?>
			</div>
		</div>

	</form>

</div><!-- container-fluid -->