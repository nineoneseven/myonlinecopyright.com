<html>
<head>
<style>
* {margin: 0; padding: 0;}
table {border-collapse: collapse; width: 100%;}
</style>
</head>
<body>
<div style="padding: 0">
<img src="<?= APPLICATION_DIR . '/templates/copyright/diploma.jpg' ?>" style="left: 0; position: absolute; top: 0; width: 100% z-index: 0;" />

<table border="0" style="margin-top: 170px; z-index: 1;">
	<tr>
		<td align="center">
			<table border="0" style="margin-bottom: 20px;">
				<tr>
					<td align="left" valign="top" width="50%" style="padding-left: 30%;">
						Copyrighted on 
						<?= $copyright->createdFriendly ?>
					</td>
					<td align="right" valign="top" width="50%" style="padding-right: 30%;">
						MOC Copyright #<?= $copyright->id ?>
					</td>
				</tr>
			</table>
			
			<table border="0">
				<tr>
					<td align="center" style="font-size: 3em;">
						<i style="color: #8da7b1;"><?= $copyright->account->name ?></i>
					</td>
				</tr>
			</table>
			
			<table border="0" style="margin-bottom: 20px;">
				<tr>
					<td align="center" style="font-size: 4em;">
						<span style="color: #507382;"><?= strlen($copyright->filename) > 20 ? substr($copyright->filename, 0, 20) . '...' : $copyright->filename ?></span>
					</td>
				</tr>
			</table>

<?php if ( $copyright->description || strlen($copyright->filename) > 20 ) : ?>
			<table border="0" style="margin-bottom: 20px;">
				<tr>
					<td align="left" valign="top" width="50%" style="padding-left: 30%;">
						<?= strlen($copyright->filename) > 20 ? 'File name: ' . $copyright->filename : '' ?>
						<?= $copyright->description ?>
					</td>
					<td width="50%">
						
					</td>
				</tr>

			</table>
<?php endif // description ?>

			<table border="0">
				<tr>
					<td align="left" valign="top" width="50%" style="padding-left: 30%;">
						MOC License number:<br />
						<?= $copyright->license ?>
					</td>
					<td width="50%">
						
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</div>
</body>
</html>