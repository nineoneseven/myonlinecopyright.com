<div class="container-fluid">

	<h1>Verify a copyright</h1>

	<hr />

<?php 
if ( isset($_GET['license']) ) :
if ( NULL == $copyright->id ) :
?>
	<p class="lead">Sorry, we coundn't find that copyright.</p>
	<p>If you think this is a mistake, please <a href="/contact">email us</a> and make sure you include your email address and the license number.</p>
<?php else : // nout found ?>

	<div class="row-fluid">
		<div class="span12">
			<h2><?= $copyright->filename ?></h2>
			<h3>Copyrighted <?=	$copyright->createdFriendly ?></h3>
			<p>Owned by <?= $copyright->account->name ?></p>
		</div>
	</div>

	<hr />


	<div class="row-fluid">
		<div class="span2">
			<h4>Type:</h4>
			<p>
			<i class="icon-<?= $copyright->icon ?>"></i>
			<?= $copyright->simpleFiletype ?>
			</p>
		</div>

		<div class="span2">
			<h4>Size:</h4>
			<p><?= $copyright->filesizeFormated ?></p>
		</div>

		<div class="span3">
			<h4>License:</h4>
			<p><?= $copyright->license ?></p>
		</div>
	</div><!-- row -->

	<hr />

	<div class="row-fluid">
		<div class="control-group span2 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
			<h4>
				Published: 
			</h4>
			<?= $copyright->ispublished ? 'Yes' : 'No' ?>
		</div>



		<div class="<?= (bool) $copyright->ispublished != TRUE ? 'hide' : '' ?> pub-toggle control-group span4 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
			<h4>
				Publication date:
			</h4>
			<p>
				<?= substr($copyright->publisheddate, 0, 1) != 0 ? date('j F, Y', strtotime($copyright->publisheddate)) : '<small>(not published)</small>' ?>
			</p>
		</div>



		<div class="<?= $copyright->ispublished != TRUE ? 'hide' : '' ?> pub-toggle control-group span4 <?= !$copyright->ismetadatacomplete ? 'error' : '' ?>">
			<h4>
				Pseudonym:
			</h4>
			<p>
				<?= $copyright->pseudonym ? $copyright->pseudonym : '<small>(none)</small>' ?>
			</p>
		</div>
	</div><!-- row -->

	<hr />

	<div class="row-fluid">
		<div class="span12">
			<h4>
				Description 
				:
			</h4>
			<p>
				<?= $copyright->description ?> 
			</p>
		</div>
	</div>

<?php endif // not found ?>



<?php else : // copyright ?>

			<form action="" method="get">
	<?php if ( $errors['form'] ) : ?>
				<div class="alert alert-error lead">
					<?= $errors['form'] ?>
				</div>
	<?php endif // form errors ?>

				<p>Have a license for a copyright, and want to verify its date? Enter it below!</p>
		
				<div class="row-fluid offset1">
					<p class="control-group span4 <?= $errors['email'] ? 'error' : '' ?>">
						<label class="lead">The copyright license:<br />
						<input type="text" name="license" placeholder="1234-1234-1234-12345" />
						<?php if ( $errors['license'] ) : ?><span class="help-block"><?= $errors['license'] ?></span><?php endif ?>
						</label>
					</p>
				</div>

				<div class="row-fluid offset1">
					<p class="span4">
						<button class="btn btn-primary" type="submit" data-loading-text="Loading...">
							Verify it!
						</button>
					</p>
				</div>

			</form>
<?php endif // copyright ?>



</div><!-- container-fluid -->