/*
 * jQuery File Upload Plugin JS Example 7.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/account/' + ACCOUNT_TOKEN + '/copyrights/'
    });



    // Load existing files:
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    })
    .done(function (result) {
        if (result.data.files.length == 0 )
        {
            $('#noFilesWell')
            .empty()
            .html('You have no files yet. Use the big &quot;Add files&quot; button to create your first copyright!');
        }



        $(this)
        .fileupload('option', 'done')
        .call(this, null, {result: result.data});
        
        $('tbody.files tr')
        .on('mouseenter', function(){
        	$(this).addClass('info');
        })
        .on('mouseleave', function(){
        	$(this).removeClass('info');
        })
        .one('click', function(e)
		{
        	var $this = $(e.target);

        	if ( $this.is('a') )
    		{
        		window.location = $this.attr('href');
        		return false;
    		}
        	
        	if ( !$this.is('tr') )
        	{
        		$this = $this.closest('tr');
        	}

        	var url = $('a.edit', $this).attr('href');
        	window.location = url;
        	return false;
        });

    });
});
