/*
 * jQuery File Upload Plugin JS Example 7.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *//*jslint nomen: true, unparam: true, regexp: true *//*global $, window, document */$(function(){"use strict";$("#fileupload").fileupload({url:"/account/"+ACCOUNT_TOKEN+"/copyrights/"});$.ajax({url:$("#fileupload").fileupload("option","url"),dataType:"json",context:$("#fileupload")[0]}).done(function(e){e.data.files.length==0&&$("#noFilesWell").empty().html("You have no files yet. Use the big &quot;Add files&quot; button to create your first copyright!");$(this).fileupload("option","done").call(this,null,{result:e.data});$("tbody.files tr").on("mouseenter",function(){$(this).addClass("info")}).on("mouseleave",function(){$(this).removeClass("info")}).one("click",function(e){var t=$(e.target);if(t.is("a")){window.location=t.attr("href");return!1}t.is("tr")||(t=t.closest("tr"));var n=$("a.edit",t).attr("href");window.location=n;return!1})})});