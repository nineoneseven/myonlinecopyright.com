$(document).ready(function(){
	$('[name=ispublished]').on('change', function(){
		var $this = $(this);
		var $row = $this.closest('.row-fluid');

		var val = $this.val();
		if ( val == 1 )
		{
			$('.pub-toggle', $row).show();
		}
		else
		{
			$('.pub-toggle', $row).hide();
			$('.pub-toggle [type=date], .pub-toggle [type=text]', $row).val('');
			$('.pub-toggle [type=radio]', $row).prop('checked', false);
			$('.pub-toggle .btn.active', $row).removeClass('active');
		}
	});
});