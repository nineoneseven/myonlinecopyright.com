// var monthNames = [ "January", "February", "March", "April", "May", "June",
//     "July", "August", "September", "October", "November", "December" ];

/* Update datepicker plugin so that MM/DD/YYYY format is used. 
 * @link http://dl.dropbox.com/u/143355/datepicker/datepicker.html
 * */
// $.extend($.fn.datepicker.defaults, {
// 	parse : function(string) {
// 		var matches;
// 		if ((matches = string.match(/^(\d{2,2})\/(\d{2,2})\/(\d{4,4})$/))) {
// 			return new Date(matches[3], matches[1] - 1, matches[2]);
// 		} else {
// 			return null;
// 		}
// 	},
// 	format : function(date) {
// 		var month = date.getMonth().toString(), 
// 		dom = date.getDate().toString();
// 		return dom + " " + monthNames[month] + ', ' + date.getFullYear();
// 	}
// });


if ( undefined !== stripe_publishable_key )
{
	Stripe.setPublishableKey(stripe_publishable_key);

	var stripeResponseHandler = function(status, response) 
	{
		var $form = $('#accountForm');

		if (response.error) 
		{
			// Show the errors on the form
			$('<div class="alert alert-error lead">' + response.error.message + '</div>').insertAfter('#fieldset-cc legend');
			$form.find('button').button('reset');
		} 
		else 
		{
			// token contains id, last4, and card type
			var token = response.id;
			// Insert the token into the form so it gets submitted to the server
			$form.append(
				$('<input type="hidden" name="stripeToken" />').val(token)
			);
		
			// and submit
			$form.get(0).submit();
		}
	};

	var choserQuantityExplainer = function(){
		var val = $(this).attr('value');
		$('#quantityExplainer .explainer:not(#quantity' + val + ')').addClass('hide');
		$('#quantityExplainer #quantity' + val).removeClass('hide');
	};
}




$(document).ready(function() 
{
	// enable disk space button group
	$('.btn-group').button();

	if ( undefined !== stripe_publishable_key )
	{

		$('#quantityWrapper input:checked').each(choserQuantityExplainer);
		$('#quantityWrapper input').on('click', choserQuantityExplainer);
		
		
		
		$('#accountForm').submit(function(event) 
		{
			var $form = $(this);
			
			$('div.alert', $form).remove();

			// Disable the submit button to prevent repeated clicks
			$form.find('button').prop('disabled', true);

			Stripe.createToken($form, stripeResponseHandler);

			// Prevent the form from submitting with the default action
			return false;
		});
	}
});