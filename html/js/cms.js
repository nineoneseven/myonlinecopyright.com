$(document).ready(function(){
	$('.cms').each(function(){
		var $cmsArea = $(this);

		if ( $cmsArea.css('position') != 'absolute' )
		{
			$cmsArea.css({
				'position' : 'relative'
			});
		}


		$('<a href="#cmsModal" role="button" class="btn btn-danger btn-cms hide" data-toggle="modal">Edit</a>')
		.css({
			'position'	: 'absolute',
			'top'		: '45%',
			'right'		: '45%'
		})
		.appendTo($cmsArea)
		.on('click', function(){
			var $content = $cmsArea.clone();
			$content.find('.btn-cms').remove();
			$('#cmsModal [name=old]').val( $.trim($content.html()) );
			$('#cmsModal textarea').val( $.trim($content.html()) );
		});



		$cmsArea
		.on('mouseenter', function(){
			$('.btn-cms', $cmsArea).show();
		})
		.on('mouseleave', function(){
			$('.btn-cms', $cmsArea).hide();
		});
	});
});