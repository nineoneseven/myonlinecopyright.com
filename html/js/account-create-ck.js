// var monthNames = [ "January", "February", "March", "April", "May", "June",
//     "July", "August", "September", "October", "November", "December" ];
/* Update datepicker plugin so that MM/DD/YYYY format is used. 
 * @link http://dl.dropbox.com/u/143355/datepicker/datepicker.html
 * */// $.extend($.fn.datepicker.defaults, {
// 	parse : function(string) {
// 		var matches;
// 		if ((matches = string.match(/^(\d{2,2})\/(\d{2,2})\/(\d{4,4})$/))) {
// 			return new Date(matches[3], matches[1] - 1, matches[2]);
// 		} else {
// 			return null;
// 		}
// 	},
// 	format : function(date) {
// 		var month = date.getMonth().toString(), 
// 		dom = date.getDate().toString();
// 		return dom + " " + monthNames[month] + ', ' + date.getFullYear();
// 	}
// });
if(undefined!==stripe_publishable_key){Stripe.setPublishableKey(stripe_publishable_key);var stripeResponseHandler=function(e,t){var n=$("#accountForm");if(t.error){$('<div class="alert alert-error lead">'+t.error.message+"</div>").insertAfter("#fieldset-cc legend");n.find("button").button("reset")}else{var r=t.id;n.append($('<input type="hidden" name="stripeToken" />').val(r));n.get(0).submit()}},choserQuantityExplainer=function(){var e=$(this).attr("value");$("#quantityExplainer .explainer:not(#quantity"+e+")").addClass("hide");$("#quantityExplainer #quantity"+e).removeClass("hide")}}$(document).ready(function(){$(".btn-group").button();if(undefined!==stripe_publishable_key){$("#quantityWrapper input:checked").each(choserQuantityExplainer);$("#quantityWrapper input").on("click",choserQuantityExplainer);$("#accountForm").submit(function(e){var t=$(this);$("div.alert",t).remove();t.find("button").prop("disabled",!0);Stripe.createToken(t,stripeResponseHandler);return!1})}});