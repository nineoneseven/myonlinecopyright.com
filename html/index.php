<?php
session_start();


error_reporting(E_ALL ^ E_NOTICE  ^ E_WARNING);
ini_set('display_errors', '1');

define('APPLICATION_DIR', __DIR__ . '/../');



require_once APPLICATION_DIR . '/environment.inc';
require APPLICATION_DIR . '/lib/Slim/Slim.php';
require APPLICATION_DIR . '/lib/rb.php';
// require_once APPLICATION_DIR . '/lib/Markdown/markdown.php';



// autoloaders
\Slim\Slim::registerAutoloader();

spl_autoload_register(function ($class) {
	$path = str_replace("_", "/", $class);
	$filename = APPLICATION_DIR . "/" . strtolower($path) . ".php";

	if (file_exists($filename)) {
		include_once $filename;
	}
});



// get config file
$config = json_decode(json_encode(Model_IniStruct::parse('../config.ini', true, APPLICATION_ENV)));
define('MANDRILL_API_KEY', $config->mandrill->apikey);



// set db
R::setup('mysql:host=' . $config->database->host . '; dbname=' . $config->database->name, 
			$config->database->user, 
			$config->database->password
);



$app = new \Slim\Slim(array(
	'debug' => $config->app->debug,
	'templates.path' => '../templates/',
	'view' => new Model_View()
));

//$app->add(new \Slim\Middleware\SessionCookie(array(
//    'expires' => '20 minutes',
//    'path' => '/',
//    'domain' => null,
//    'secure' => false,
//    'httponly' => false,
//    'name' => 'slim_session',
//    'secret' => 'my1online2copyright3',
//    'cipher' => MCRYPT_RIJNDAEL_256,
//    'cipher_mode' => MCRYPT_MODE_CBC
//)));

// $app->add(new \Slim\Middleware\Flash(array(
// )));



// http://baylorrae.com/add-flash-messages-to-your-site/
// if $_SESSION['flash_messages'] isset
// then save them to our class
if( isset($_SESSION['flash_messages']) ) {
  Model_Flash::$messages = $_SESSION['flash_messages'];
}

// reset the session's value
$_SESSION['flash_messages'] = array();



// add config to app
$app->config = $config;



// ROUTES

// $app->get('/', array(new Controller_Public(), 'temp'));
$app->get('/', array(new Controller_Public(), 'index'));

$app->get('/test123/', array(new Controller_Public(), 'test'));


/*
 * APP dashboard
 */
$app->get('/app/', array(new Controller_App(), 'index'));



/*
 * ACCOUNT sign in (get token)
 * @param	email
 * @param	password
 */
$app->map('/signin/', array(new Controller_Account(), 'signin'))
->via('GET', 'POST');



/*
 * ACCOUNT sign out
 */
$app->get('/account/signout/', array(new Controller_Account(), 'signout'));



/*
 * ACCOUNT register new account
 * @param	email
 * @param	password
 */
$app->map('/account/register/', array(new Controller_Account(), 'create'))
->via('GET', 'POST');



/*
 * ACCOUNT reset password
 */
$app->map('/account/password',  array(new Controller_Account(), 'password' ))
->via('GET', 'POST');



/*
 * ACCOUNT get settings
 */
//$app->post('/account/:accountToken/',  array(new Controller_Account(), 'settings' ));



/*
 * ACCOUNT update
 */
$app->map('/account/', array(new Controller_Account(), 'update'))
->via('GET', 'POST');



/*
 * ACCOUNT delete
 */
//$app->post('/account/:accountToken/delete', array(new Controller_Account(), 'delete'));



/*
 * COPYRIGHT get all
 */
$app->get('/account/:accountToken/copyrights/', array(new Controller_Copyright(), 'all'));



/*
 * COPYRIGHT get one
 */
$app->get('/copyrights/:copyrightId/', array(new Controller_Copyright(), 'one'));



/*
 * COPYRIGHT download
 */
$app->get('/copyrights/:copyrightId/download/', array(new Controller_Copyright(), 'download'));



/*
 * COPYRIGHT simple html for pdf
 */
$app->get('/copyrights/:copyrightId/forpdf/', array(new Controller_Copyright(), 'forpdf'));



/*
 * COPYRIGHT certificate
 */
$app->get('/copyrights/:copyrightId/certificate/', array(new Controller_Certificate(), 'create'));


/*
 * COPYRIGHT create
 * @param	filename
 */
$app->post('/account/:accountToken/copyrights/', array(new Controller_Copyright(), 'create')); // createText



/*
 * COPYRIGHT update
 * @param	id
 */
$app->map('/copyrights/:copyrightId/edit/', array(new Controller_Copyright(), 'update'))
->via('GET', 'POST');


/*
 * COPYRIGHT delete
 * @param	id
 */
$app->get('/copyrights/:copyrightId/delete/', array(new Controller_Copyright(), 'delete'));




/*
 * COPYRIGHT verification
 * @param	license
 */
$app->get('/copyright/verification/', array(new Controller_Copyright(), 'verify'));
$app->get('/copyright/verification/:license/', array(new Controller_Copyright(), 'verify'));



$app->get('/terms-service/', array(new Controller_Public(), 'terms'));

$app->get('/privacy-policy/', array(new Controller_Public(), 'privacy'));

$app->get('/frequently-asked-questions/', array(new Controller_Public(), 'faq'));

$app->map('/contact-us/', array(new Controller_Public(), 'contact'))
->via('GET', 'POST');


/*
 * Admin routes
 */
$app->get('/admin/', array(new Controller_Admin(), 'index'));

$app->map('/admin/cms/', array(new Controller_Admin(), 'cms'))
->via('GET', 'POST');

$app->get('/admin/logs/', array(new Controller_Admin(), 'logs'));



/*
 * system routes
 */
$app->post('/system/stripe/webhooks/', array(new Controller_System(), 'stripe_webhooks'));

$app->get('/system/aws/s3/push/', array(new Controller_System(), 'push_to_s3'));



$app->error(function (\Exception $e) use ($app) 
{
	$app->render('public/error.php', array('message' => $e->getMessage() ));
});



$app->notFound(array(new Controller_Public(), 'notFound'));



$app->run();