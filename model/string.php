<?php

class Model_String
{
	static function parseKey($token, $length = 32)
	{
		if ( strlen($token) <= $length )
		{
			throw new Exception('Token is invalid length');
		}

		$return = NULL;
		$return->token = substr($token, -$length);
		$return->id = substr($token, 0, strlen($token) - $length);

		if ( strlen($return->token) !== $length )
		{
			throw new Exception('Returned token is invalid length');
		}

		if ( !is_numeric($return->id) )
		{
			throw new Exception('Returned ID is not an integer');
		}

		return $return;
	}
	
	
	
	static function get_emails($str) 
	{
		$emails = array ();
		preg_match_all ( "/\b\w+\@\w+[\.\w+]+\b/", $str, $output );
		foreach ( $output [0] as $email )
			array_push ( $emails, strtolower ( $email ) );
		if (count ( $emails ) >= 1)
			return $emails;
		else
			return false;
	}

	
	
	static function markdown_and_obfuscate($string)
	{
		$string = markdown ( $string );

		// find email links
		// http://www.codewalkers.com/c/a/Email-Code/Get-Email-Addresses-from-Strings/
		
		
		$emails = self::get_emails ( $string );
		
		// replace emails with ascii
		foreach ( $emails as $email ) 
		{
			$newEmail = '';
			for($i = 0; $i < strlen ( $email ); $i ++) 
			{
				$newEmail .= "&#" . ord ( $email [$i] ) . ";";
			}
			
			$string = str_replace ( $email, $newEmail, $string );
		}
	
		// add _blank to links
		$string = str_replace ( 'a href="http', 'a target="_blank" href="http', $string );
		
		return $string;
	}
}