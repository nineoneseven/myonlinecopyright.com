<?php
/*
 * @url	http://stackoverflow.com/questions/4752177/php-simple-template-engine
 */
class Model_Template 
{
	private $templateFile;
	
	public function __construct($templateFile) 
	{
		if (! is_file ( $templateFile )) 
		{
			throw new exception ( 'Could not load template file \'' . $templateFile . '\'' );
		}
		
		$this->templateFile = $templateFile;
	}
	
	public function render($vars = array()) 
	{
		ob_start ();
		extract ( $vars );
		require ($this->templateFile);
		$contents = ob_get_contents ();
		ob_end_clean ();
		return $contents;
	}
}