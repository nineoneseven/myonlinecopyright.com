<?php
class Model_Account extends RedBean_SimpleModel 
{
	public function dispense()
	{
		$this->email = '';
		$this->password = '';
		$this->name = '';
		$this->dateofbirth = '';
//		$this->cityofbirth = '';
		$this->countryofbirth = '';
		$this->created = date('Y-m-d H:i:s');
		$this->token = md5(rand());
		$this->isactive = 1;
		$this->isadmin = 0;
		$this->isverified = md5(rand());
		
		$this->files_count = 0;
		$this->files_diskspaceused = 0;

		$this->stripe_customer_id = '';
		// $this->stripe_issubscribed = 0;
		$this->stripe_subscription_quantity = 1;
		$this->stripe_cancelled = '0000-00-00 00:00:00';
		$this->deleted = '0000-00-00 00:00:00';
		
	}



	static function getDiskSpaceRemaining($account)
	{
		$diskspaceAllowedInKb = $account->stripe_subscription_quantity * 1000 * 1000 * 1000; // plans * gb * mb * kb
		return $diskspaceAllowedInKb - $account->files_diskspaceused;
	}



	static function getFromToken($token)
	{
		$tokenArr = explode('-', $token);


		$accountToken= Model_String::parseKey($tokenArr[0]);



		// get account
		try
		{
			$account = R::findOne(
				'account',
				' id = ? AND isactive = 1',
				array($accountToken->id)
			);
		}
		catch (Exception $e)
		{
			throw new Exception('account not found', 401);
		}



		if ( !$account->id )
		{
			throw new Exception('account not found', 401);
		}



		if ( $account->token != $accountToken->token )
		{
			throw new Exception('account token does not match', 401);
		}



		// verify expiration
		$signinToken = Model_String::parseKey($tokenArr[1]);

		$signin = $account->ownSignin[$signinToken->id];

		if ( empty($signin) || !$signin->isactive )
		{
			throw new Exception('account signin not found', 401);
		}



		if ( strtotime($signin->expire) < strtotime('now') )
		{
			$signin->isactive = 0;

			try
			{
				R::store($signin);
			}
			catch (Exception $e)
			{
				// email admin
			}

			throw new Exception('account signin expired', 401);
		}



		return $account;
	}
	
	
}


