<?php

class Model_Copyright extends RedBean_SimpleModel 
{
	// called on dispense
	public function dispense ()
	{
		$this->filename = '';
		$this->filehashsha256 = '';
		$this->filehashmd5 = '';
		$this->filetype =  '';
		
		$this->description =  '';
		$this->ispublished = 0;
		$this->publisheddate = '0000-00-00 00:00:00';
		$this->isoriginal = 1;
		$this->pseudonym = '';
		$this->ismetadatacomplete = 0;
		
		$this->license = '';
		$this->filesize = '';
		$this->created = date('Y-m-d H:i:s');
		$this->deleted = '0000-00-00 00:00:00';
		$this->isactive = 1;

		$this->isstoredremotely = 0;
		$this->s3storedtime = 0;
		$this->s3hash = 0;
	}
	
	
	
	public function formatProperties($properties)
	{
		$properties['createdFriendly'] = date('j F, Y', strtotime($this->created));
		$properties['createdUnix'] = date('U', strtotime($this->created));
		$properties['filesizeFormated'] = $this->formatFileSize();
		$properties['icon'] = $this->getIconFromMimeType();
		$properties['simpleFiletype'] = $this->getFileTypeFromMimeType();
		$properties['descriptionShort'] = $this->shortenString($this->description, 42, FALSE);
		$properties['filenameShort'] = $this->shortenString($this->filename, 20, TRUE);
		$properties['ismetadatacomplete'] = (bool) $this->ismetadatacomplete;

		return $properties;
	}



	public function shortenString($str, $length = 12, $includeExtension = TRUE)
	{
		if (strlen($str) < $length) return $str;
		
		// get extension
		// http://stackoverflow.com/questions/10368217/php-get-file-extension
		if ( $includeExtension )
		{
			$ext = pathinfo($str, PATHINFO_EXTENSION);
		}
		if ( strlen($str) > $length - 3 - strlen($ext) )
		{
			$str = substr($str, 0, $length - 3) . '...' . $ext;
		}

		return $str;
	}



	public function formatFileSize() 
	{
		return self::getFormattedFileSize($this->filesize);
	}
	
	
	
	static function getFormattedFileSize($filesize)
	{
		if ($filesize >= 1000000000) {
			return round($filesize / 1000000000, 2) . ' GB';
		}
		if ($filesize >= 1000000) {
			return round($filesize / 1000000, 2) . ' MB';
		}
		return round($filesize / 1000, 2) . ' KB';
	}
	
	
	
	public function getIconFromMimeType() 
	{
		if (empty ( $this->filetype )) {
			return 'file';
		}
		
		$fileType = $this->getFileTypeFromMimeType ();
		
		switch ($fileType) {
			case 'image' :
				return 'camera';
			case 'audio' :
				return 'music';
			case 'video' :
				return 'film';
			case 'text' :
			case 'pdf' :
				return 'book';
			default :
				return 'file';
		} // switch	
	}
	
	
	
	public function getFileTypeFromMimeType() 
	{
		if (empty ( $this->filetype )) {
			return 'file';
		}
		
		$typeArr = explode ( '/', $this->filetype );
		
		switch ($typeArr [1]) {
			case 'pdf' :
				return 'pdf';
		} // switch	
		

		switch ($typeArr [0]) {
			case 'image' :
				return 'image';
			case 'audio' :
				return 'audio';
			case 'video' :
				return 'video';
			case 'text' :
				return 'text';
			default :
				return 'file';
		} // switch	
	}
}

