<?php

class Model_Json
{
	public $http_response_code = 200;
	public $success = FALSE;
	
	
	
	public function __toString()
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		
//		http_response_code($this->http_response_code);
		header(':', true, $this->http_response_code);

		unset($this->http_response_code);
		
		if ( !isset($this->error) )
		{
			$this->success = TRUE;
		}
		

		
		// for debugging
		if ( isset($_GET['timer']) )
		{
			$time_end = microtime_float();
			$time = $time_end - SCRIPT_START_TIME ;
			
			$this->debug->script->timeInSeconds = round($time, 3);
		}



		echo json_encode($this) . "\n";
		exit;
	}
	
	
	
	public function error($error = 'General error', $code = 500)
	{
		$this->http_response_code = $code;
		
		$this->error = $error;
		
		echo $this;
	}
}