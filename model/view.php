<?php


class Model_View extends \Slim\View
{
	private $pageId;
	private $pageClassList;
	
	public function __construct()
	{

	}
	
	
	public function render($template)
    {
    	if ( !$this->data['useLayout'] )
    	{
    		echo parent::render($template, $this->data);
    		exit;
    	}

    	// render body content
		$body = parent::render($template, $this->data);

		// render layout
		$this->setIdFromTemplatePath($template);
    	
    	parent::appendData(array(
	    	'body'				=> $body,
	    	'pageId'			=> $this->pageId,
    		'pageClassList'		=> $this->pageClassList
    	));

    	echo parent::render('layout.php');
    	
    	unset( $_SESSION['slim.flash'] );
    	exit;
    }


    
    public function setIdFromTemplatePath($template)
    {
		// remove extension
		$template = preg_replace("/\\.[^.\\s]{3,4}$/", "", $template);
    	
		$pathArr = explode('/', $template);

		$str = '';
		$pathRebuild = '';
		foreach ( $pathArr as $path )
		{
			$pathRebuild .= $path . '-';
			$str .= 'page-' . rtrim($pathRebuild, '-') . ' ';
		}

		$this->pageClassList = trim($str);
		
		$classArr = explode(' ', $this->pageClassList);
		
		$this->pageId = array_pop($classArr);
    }
    
}


