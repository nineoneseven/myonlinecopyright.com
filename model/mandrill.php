<?php

class Model_Mandrill
{
	private $data;
	
	public function __construct()
	{
		require_once '../lib/Mandrill.php';
		
		$this->data = new stdClass;
		
		$this->data->type = 'messages';
		$this->data->call = 'send';
		
		$this->data->message = new stdClass;
		
		$this->data->message->track_opens = FALSE;
        $this->data->message->track_clicks = FALSE;
        $this->data->message->auto_text = true;
        $this->data->message->async = true;

		$this->data->message->to = array();
		
//		$mail->message->text = 'test text';
//		$mail->message->subject = 'test subject';
//		$mail->message->from_email = 'corey@hirethere.com';
//		$mail->message->from_name = 'corey, hirethere.com';
		
		
		
		
        
//        $mail->message->tags = array('test');

//		$request_json = '{"type":"messages","call":"send","message":{"html": "<h1>example html</h1>", "text": "example text", "subject": "example subject", "from_email": "wes@werxltd.com", "from_name": "example from_name", "to":[{"email": "wes@reasontostand.org", "name": "Wes Widner"}],"headers":{"...": "..."},"track_opens":true,"track_clicks":true,"auto_text":true,"url_strip_qs":true,"tags":["test","example","sample"],"google_analytics_domains":["werxltd.com"],"google_analytics_campaign":["..."],"metadata":["..."]}}';

	}
	
	
	
	public function send()
	{
		try 
		{
			$response = Mandrill::call((array) $this->data);
			
		} 
		catch (Exception $e) 
		{
			throw new Exception('mail couldn\'t be sent: ' . $e->getMessage() );
		}
		
		$responseObj = json_decode($response);
		
		if ( $responseObj->status == 'error' )
		{
			throw new Exception('mail couldn\'t be sent: ' . $responseObj->message );
		}
		
		return $responseObj;
	}



	public function addRecipient($email, $name = '')
	{
		$to = new stdClass;
		$to->email = $email;
		
		if ( $name )
		{
			$to->name = $name;
		}
		
		$this->data->message->to[] = $to;
	}
	
	
	
	public function __set($name, $value)
	{
		$this->data->message->$name = $value;
	}
	
}


