<?php

class Controller_Copyright extends Controller_Controller
{
	public function one($copyrightId)
	{
		parent::requireSignedIn();



		$copyright = $this->app->account->ownCopyright[$copyrightId];
		
		if ( !$copyright ) $this->app->notFound();


		$copyrightData = $copyright->getProperties();

		// if incomplete, force them to the edit page
		if ( !$copyrightData['ismetadatacomplete'] )
		{
			$this->app->redirect('/copyrights/' . $copyrightData['id'] . '/edit');
		}



		$this->app->viewData->copyright = (object) $copyright->formatProperties($copyrightData);
		
		$this->render();
	}



	public function download($copyrightId)
	{
		parent::requireSignedIn();


		
		$copyright = $this->app->account->ownCopyright[$copyrightId];
		
		if ( !$copyright ) $this->app->notFound();



		// pull from s3
		if ( $copyright->isstoredremotely )
		{
			// https://github.com/tpyo/amazon-s3-php-class
			require_once(APPLICATION_DIR . 'lib/tpyo-amazon-s3-php-class-2061fa8/S3.php');

			$bucketName = $this->app->config->aws->s3->bucket;
			$fileName = $copyright->account_id . '-' . $copyright->id;

			// Instantiate the class
			$s3 = new S3 (
				$this->app->config->aws->s3->key, 
				$this->app->config->aws->s3->secret
			);



			// update headers to be file name
			// http://undesigned.org.za/2007/10/22/amazon-s3-php-class/documentation#copyObject
			$response = S3::copyObject(
				$bucketName, // from
				$fileName, // from
				$bucketName, // to
				$fileName, // to
				S3::ACL_AUTHENTICATED_READ,
		        array(), // AMZ header; x-amz-uid
		        array('Content-Disposition' => 'attachment; filename="' . $copyright->filename . '"') // New content type
		    );



			// create authenticated URL
			$url = $s3->getAuthenticatedURL($bucketName, $fileName, 60);

			$this->app->redirect($url);

			exit;
		}



		$filePath = $this->app->config->app->file_folder->path . $this->app->account->id . '-' . $copyright->id;

		
		
		// required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression'))
		{
			ini_set('zlib.output_compression', 'Off');
		}
		
		

		// http://davidwalsh.name/php-file-extension
		$file_extension = substr(strrchr($copyright->filename,'.'),1);;
		
		switch( $file_extension )
		{
			case "pdf": $ctype="application/pdf"; break;
			case "exe": $ctype="application/octet-stream"; break;
			case "zip": $ctype="application/zip"; break;
			case "doc": $ctype="application/msword"; break;
			case "xls": $ctype="application/vnd.ms-excel"; break;
			case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpg"; break;
			default: $ctype="application/force-download";
		}
		
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers
		header("Content-Type: $copyright->filetype");
		// change, added quotes to allow spaces in filenames, by Rajkumar Singh
		header("Content-Disposition: attachment; filename=\"" . $copyright->filename . "\";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($filePath));
		readfile("$filePath");
		exit();
	}



	public function all($accountToken)
	{
		$json = new Model_Json();
		
		try
		{
			$account = Model_Account::getFromToken($accountToken);
		}
		catch (Exception $e)
		{
			$json->error($e->getMessage(), $e->getCode());
		}



		$copyrights = $account->withCondition('isactive = 1 ORDER BY id ASC')->ownCopyright;

		$copyrightArr = array();
		foreach ( $copyrights as $copyright)
		{
			$copyrightData = $copyright->getProperties();
			$copyrightArr[] = (object) $copyright->formatProperties($copyrightData);
		}


		$json->data['files'] = $copyrightArr;

		echo $json;

	}



	public function create ($accountToken)
	{
		$json = new Model_Json();
		
		
		
		try
		{
			$account = Model_Account::getFromToken($accountToken);
		}
		catch (Exception $e)
		{
			$this->json->error($e->getMessage(), $e->getCode());
		}

		

		set_time_limit(0);
		ini_set('memory_limit', '128M');



		$uploadFolderPath = $this->app->config->app->file_folder->path;

		

		$options = array(
			'upload_dir'			=> $uploadFolderPath,
			'upload_url'			=> $this->request->getPath(),
			'script_url'			=> $this->request->getPath(),
			'delete_type'			=> 'POST',
//			'accept_file_types'		=> '/\.(gif|jpe?g|png)$/i'
		);



		// capture content, so we can mess w image before returning content 
		ob_start ();
		$upload_handler = new Model_UploadHandler($options);
		$contents = ob_get_contents ();
		ob_end_clean ();



		$json = json_decode($contents);


		$fileInfo = $json->files[0];

		if ( $json->error )
		{
			echo $contents;
			exit;
		}



		$filePath = $uploadFolderPath .'/' . $fileInfo->name;



		$copyright = R::dispense('copyright');

		$copyright->filename = $fileInfo->name;
		$copyright->filehashsha256 = hash_file('sha256', $filePath);
		$copyright->filehashmd5 = hash_file('md5', $filePath);
		$copyright->license = substr(md5(date('U')), 0, 4) . '-' . substr($copyright->filehashsha256, 0, 4) . '-' . substr(md5(rand()), 0, 4) . '-' . substr(md5(rand()), 0, 4);
		$copyright->filetype =  $fileInfo->type;
		$copyright->filesize = $fileInfo->size;

		$account->files_count = $account->files_count + 1;
		$account->files_diskspaceused = $account->files_diskspaceused + $copyright->filesize;
		
		$account->ownCopyright[] = $copyright;



		try
		{
			R::store($account);
		}
		catch (Exception $e)
		{
			$this->json->error($e->getMessage(), $e->getCode());
		}



		try 
		{
			rename(
				$uploadFolderPath . '/' . $fileInfo->name,
				$uploadFolderPath . '/' . $account->id . '-' . $copyright->id
			);
		}
		catch (Exception $e)
		{
			$this->json->error($e->getMessage(), $e->getCode());
		}

		
		
		$copyrightData = $copyright->getProperties();
		$copyright = (object) $copyright->formatProperties($copyrightData);
		
		$data->files = array($copyright);

		// return json string
		echo json_encode($data);
		exit;
	}



	public function update ($copyrightId)
	{
		parent::requireSignedIn();



		$this->app->viewJs[] = '/js/copyright-one-edit-ck.js';



		$copyright = $this->app->account->ownCopyright[$copyrightId];
		
		if ( !$copyright ) $this->app->notFound();
		
		
		
		$this->app->viewData->editing = TRUE;
		$copyrightData = $copyright->getProperties();

		// if incomplete, show alert
		if ( !$copyrightData['ismetadatacomplete'] )
		{
			Model_Flash::$messages['error'] = 'This copyright needs a little love. Please fill out the fields in red below.';
		}

		$this->app->viewData->copyright = (object) $copyright->formatProperties($copyrightData);
		
		
		
		if ( !$this->request->isPost() ) $this->render(array(), 'copyright/one');



		// validate input
//		$validator = new Model_Validator( $this->post );
//
//		$validator->required ( 'You must supply a filename' )
//			->validate ( 'description' );
//
//		// check for errors
//		if ($validator->hasErrors ())
//		{
//			$this->json->error($validator->getAllErrors (), 400);
//		}



		$copyright->ispublished = $this->post['ispublished'];

		$copyright->publisheddate = date('Y-m-d', strtotime($this->post['day'] . ' ' . $this->post['month'] . ' ' . $this->post['year']));;
		$copyright->isoriginal = $this->post['isoriginal'];
		$copyright->pseudonym = $this->post['pseudonym'];
		$copyright->description = $this->post['description'];

		$_filesize = $copyright->filesize;

		if ($_filesize >= 1000000000) {
			$filesize = round($_filesize / 1000000000, 2) . ' GB';
		} elseif ($_filesize >= 1000000) {
			$filesize = round($_filesize / 1000000, 2) . ' MB';
		} else { $filesize = round($_filesize / 1000, 2) . ' KB'; }

		// they've updated everything
		$copyright->ismetadatacomplete = 1;

		if($copyright->ispublished == 1) { $published = 'Yes'; } else { $published = 'No'; }

		// --- EMAIL ---                 
		// Get Attachment
		require_once(APPLICATION_DIR . "/lib/dompdf-master/dompdf_config.inc.php");     
		// get html
		$htmlToRender = file_get_contents($this->request->getUrl() . '/copyrights/' . $copyright->id . '/forpdf');
		// create pdf
		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlToRender);
		$dompdf->set_paper('A4', 'landscape');
		$dompdf->render();
		$pdfStr = $dompdf->output();
		// save, send, delete
		$output_file = $this->app->config->app->for_printing->path . '/copyright-' . $copyright->id . '.pdf';
		file_put_contents($output_file, $pdfStr);

		/* Email Detials */
		$mail_to = $this->app->account->email;
		$from_mail = $this->app->config->app->email->from_email;
		$from_name = $this->app->config->app->email->from_name;
		$reply_to = $this->app->config->app->email->from_email;
		$subject = 'Your copyright has been updated';
		$message = '<p>Dear '.$this->app->account->name.',</p><p>Your copyright details are:</p><p>
                         File name: '.$copyright->filename.'<br />
                         Copyrighted: '.date('j F, Y', strtotime($copyright->created)).'<hr />
                         Type: '.$copyright->filetype.'<br />
                         Size: '.$filesize.'<br />
                         License: '.$copyright->license.'<hr />
                         Published: '.$published.'<br />
                         Publication date: '.date('j F, Y', strtotime($copyright->publisheddate)).'<br />
                         Pseudonym: '.$copyright->pseudonym.'<br />
                         Description: '.$copyright->description.'</p>
                         <p>Thank you for choosing MyOnlineCopyright.com,<br />- MOC Staff -</p>';

		/* Attachment File */
		// Attachment location
		$file_name = 'copyright-' . $copyright->id .'.pdf';
		$path = $this->app->config->app->for_printing->path;

		// Read the file content
		$file = $path.'/'.$file_name;
		$file_size = filesize($file);
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		$content = chunk_split(base64_encode($content));

		/* Set the email header */
		// Generate a boundary
		$boundary = md5(uniqid(time()));

		// Email header
		$header = "From: ".$from_name." <".$from_mail.">".PHP_EOL;
		$header .= "Reply-To: ".$reply_to.PHP_EOL;
		$header .= "MIME-Version: 1.0".PHP_EOL;

		// Multipart wraps the Email Content and Attachment
		$header .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"".PHP_EOL;
		$header .= "This is a multi-part message in MIME format.".PHP_EOL;
		$header .= "--".$boundary.PHP_EOL;

		// Email content
		// Content-type can be text/plain or text/html
		$header .= "Content-type:text/html; charset=iso-8859-1".PHP_EOL;
		$header .= "Content-Transfer-Encoding: 7bit".PHP_EOL.PHP_EOL;
		$header .= "$message".PHP_EOL;
		$header .= "--".$boundary.PHP_EOL;

		// Attachment
		// Edit content type for different file extensions
		$header .= "Content-Type: application/xml; name=\"".$file_name."\"".PHP_EOL;
		$header .= "Content-Transfer-Encoding: base64".PHP_EOL;
		$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"".PHP_EOL.PHP_EOL;
		$header .= $content.PHP_EOL;
		$header .= "--".$boundary."--";

		try
		{
			if (mail($mail_to, $subject, "", $header)) {
				unlink($output_file);
			}
			R::store($copyright);
		}
		catch (Exception $e)
		{
			$this->json->error($e->getMessage(), $e->getCode());
		}


		Model_Flash::add('success', 'Great, your copyright has been updated.');
		$this->app->redirect('/copyrights/' . $copyrightId);
	}



	public function delete ($copyrightId)
	{
		parent::requireSignedIn();



		$copyright = $this->app->account->ownCopyright[$copyrightId];
		
		if ( !$copyright ) $this->app->notFound();



		$filePath = $this->app->config->app->file_folder->path . $this->app->account->id . '-' . $copyright->id;
		unlink($filePath);



		$copyright->isactive = 0;
		$copyright->deleted = date('Y-m-d H:i:s');

		$this->app->account->files_diskspaceused = $this->app->account->files_diskspaceused - $copyright->filesize;
		$this->app->account->files_count = $this->app->account->files_count - 1;
		


		try
		{
			R::store($this->app->account);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		$this->app->redirect('/app');
	}
	
	
	
	public function forpdf($copyrightId)
	{
	// get account
		try
		{
			$copyright = R::findOne(
				'copyright',
				' id = ? AND isactive = 1',
				array($copyrightId)
			);
		}
		catch (Exception $e)
		{
			$this->app->notFound($e);
		}

		
		
		if ( !$copyright ) $this->app->notFound();

		
		$copyright->account = $copyright->account;
		
		$copyrightData = $copyright->getProperties();
		$this->app->viewData->copyright = (object) $copyright->formatProperties($copyrightData);
		
		$this->app->useLayout = FALSE;

		
		
		$this->render();
	}



	public function verify()
	{
		if ( !isset($_GET['license']) ) $this->render();

		$license = $_GET['license'];

		$copyright = R::findOne(
			'copyright',
			' license = ? ',
			array($license)
		);

		// $copyrightData = $copyright->getProperties();
		if ( !is_null($copyright) )
		{
	  		$copyright = (object) $copyright->formatProperties($copyright);
	  	}

		$this->app->viewData->copyright = $copyright;

		$this->render();
	}
}


