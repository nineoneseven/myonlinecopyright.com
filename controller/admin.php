<?php

class Controller_Admin extends Controller_Controller
{
	public function index()
	{
		parent::requireAdmin();

		$this->render();
	}



	public function cms ()
	{
		parent::requireAdmin();



		if ( !$this->app->request()->isPost() ) exit();

		require_once APPLICATION_DIR . '/model/simple_html_dom.php';
		
		$templateDir = $this->app->config('templates.path');
		$templatePath = APPLICATION_DIR . str_replace('../', '', $templateDir) . $this->post['template'] . '.php';

		if ( !is_file($templatePath) )
		{
			try
			{
				throw new Exception('template file not found');
			}
			catch (Exception $e)
			{
				$this->app->error($e);
			}
		}



		// http://simplehtmldom.sourceforge.net/manual.htm#section_find
		$html = file_get_html($templatePath);

		$cmsAreas = $html->find('*[class=cms]'); 

		foreach ($cmsAreas as $key => $cmsArea)
		{

			$found = trim(preg_replace( "/\r|\n|\t/", "", $cmsArea->innertext()));
			$found = str_replace('> <', '><', $found);

			$toFind = trim(preg_replace( "/\r|\n|\t/", "", $this->post['old']));

			if ( $found == $toFind )
			{
				break;
			}
		}

		$html->find('*[class=cms]', $key)->innertext = $this->post['new']; 
		$newHtml = $html->save();



        // back up the existing file
		$newfile = $templatePath . '.bk.' . date('Y-d-m_U');

		if (!copy($templatePath, $newfile)) 
		{
		    try 
		    {
				throw new Exception("failed to copy file");
		    } catch (Exception $e) 
			{
				$this->app->error($e);
			}
		}


        
		// save the new file
		$fp = fopen($templatePath, 'w');
		if ( $fp === FALSE ) 
		{
			try 
		    {
				throw new Exception("Couldnt load page to save");
		    } catch (Exception $e) 
			{
				$this->app->error($e);
			}
		}



		if (flock($fp, LOCK_EX)) 
		{ // do an exclusive lock
			ftruncate($fp, 0); // truncate file
			fwrite($fp, $newHtml);
			flock($fp, LOCK_UN); // release the lock
		} 
		else 
		{
			exit('error writing new file');
		}

		fclose($fp);



		$this->app->redirect($this->post['referer']);
	}



	public function logs ()
	{
		parent::requireAdmin();
		


		$this->app->viewData->path = $this->request->getPath();

		$logsPath = '../data/logs/';



		// get list of files for editing
		$files = scandir($logsPath);

		foreach ($files as $file) 
		{
			if ( substr($file, 0, 1) == '.' ) continue;

			$filesArr[] = $file;
		}

		$this->app->viewData->filesArr = $filesArr;



		if ( isset($_GET['file']) )
		{
			$this->app->viewData->content = file_get_contents(realpath($logsPath . '/' . $_GET['file']));
		}
		
		$this->render();
	}
}


