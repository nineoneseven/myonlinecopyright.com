<?php

class Controller_Account extends Controller_Controller
{
	public function create()
	{
		$this->app->viewCss[] = '/bootstrap/css/bootstrap-datepicker.css';
		$this->app->viewCss[] = '/css/account-create.css';
		
		$this->app->viewJs[] = 'https://js.stripe.com/v1/';
		$this->app->viewJs[] = '/bootstrap/js/bootstrap-datepicker.js';
		$this->app->viewJs[] = '/js/account-create-ck.js';

		$this->app->viewData->stripe_publishable_key = $this->app->config->stripe->publishable_key;
		
		
		
		if ( !$this->request->isPost() ) $this->render();



		// validate input
		$validator = new Model_Validator( $this->post );
		
		$validator
			->required('Please select a quantity')
			->validate('quantity');

		$validator->required ( 'You must supply a password.' )
			->minLength (4, 'Password must be longer than 4 characters')
			->maxLength (32, 'Password must be shorter than 32 characters')
			->validate ( 'password' );

		$validator
			->required('You must supply an email address.')
			->email('You must supply a valid email address')
			->validate('email');
			
		// $validator
		// 	->required('You must supply a date of birth (required)')
		// 	->date('You must supply a valid date of birth (date)')
		// 	->validate('dateofbirth');

		$validator
			->required('You must supply a name (required)')
			->minLength (4, 'name must be longer than 4 characters')
			->maxLength (64, 'name must be shorter than 32 characters')
			->validate('name');

		$validator
			->required('You must agree with our Terms and Conditions')
			->validate('tos');
			
//		$validator
//			->required('You must supply a city of birth (required)')
//			->minLength (4, 'city of birth must be longer than 4 characters')
//			->maxLength (32, 'city of birth must be shorter than 32 characters')
//			->validate('cityofbirth');

		$validator
			->required('You must supply a country of birth (required)')
//			->minLength (4, 'country of birth must be longer than 4 characters')
//			->maxLength (32, 'country of birth must be shorter than 32 characters')
			->validate('countryofbirth');



		// check for errors
		if ($validator->hasErrors ())
		{
			$data = $this->post;
			$data['errors'] = $validator->getAllErrors();
			$data['errors']['cc_number'] = 'For safety, re-enter your card number';
			$data['errors']['cc_cvc'] = 'and CVC';

			$this->render($data);
		}



		try
		{
			$dupeAccount = R::findOne(
				'account',
				' email = ? ',
				array($this->post['email'])
			);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}
		


		if ($dupeAccount->id > 0)
		{
			$data = $this->post;
			$data['errors']['email'] = 'An account already exists for that email address';
			$data['errors']['cc_number'] = 'For safety, re-enter your card number';
			$data['errors']['cc_cvc'] = 'and CVC';
			$this->render($data);
		}


		
		// process payment
		require_once(APPLICATION_DIR . 'lib/Stripe/Stripe.php');

		Stripe::setApiKey($this->app->config->stripe->secret_key);
		
		if($this->post['quantity'] == 1) {
			$qname = $this->app->config->stripe->plans->one->name;
		}elseif($this->post['quantity'] == 2) {
			$qname = $this->app->config->stripe->plans->two->name;
		}elseif($this->post['quantity'] == 3) {
			$qname = $this->app->config->stripe->plans->three->name;
		}elseif($this->post['quantity'] == 5) {
			$qname = $this->app->config->stripe->plans->five->name;
		}elseif($this->post['quantity'] == 10) {
			$qname = $this->app->config->stripe->plans->ten->name;
		}elseif($this->post['quantity'] == 25) {
			$qname = $this->app->config->stripe->plans->twentyfive->name;
		}

		$customerInfo = array(
			'card'		=> $this->post['stripeToken'],
			'plan'		=> $qname,
			'email'		=> $this->post['email'],
			#'quantity'	=> $this->post['quantity']
			'quantity'	=> 1,
		);

		if ( isset($this->post['coupon']) && !empty($this->post['coupon']) )
		{
			$customerInfo['coupon'] = $this->post['coupon'];
		}

		try
		{
			$customer = Stripe_Customer::create($customerInfo);
		}
		catch (Exception $e)
		{
			$trace = $e->getTrace();

			$data = $this->post;
			$data['errors']['cc_number'] = 'For safety, re-enter your card number';
			$data['errors']['cc_cvc'] = 'and CVC';

			$data['errors']['form'] = $e->getMessage();

			if ( isset($trace[0]['args'][0]) )
			{
				$args = json_decode($trace[0]['args'][0]);

				if ( is_object($args) && isset($args->error->param) )
				{
					$data['errors'][$args->error->param] = $args->error->message;
				}
			}

			$this->render($data);
		}


		// create acount
		$account = R::dispense('account');
		
		$account->email = $this->post['email'];
		$account->password = md5($this->post['password']);
		$account->name = $this->post['name'];
		$account->dateofbirth = date('Y-m-d', strtotime($this->post['day'] . ' ' . $this->post['month'] . ' ' . $this->post['year']));
//		$account->cityofbirth = $this->post['cityofbirth'];
		$account->countryofbirth = $this->post['countryofbirth'];
		$account->stripe_customer_id = $customer->id;
		// $account->stripe_issubscribed = 1;
		$account->stripe_subscription_quantity = $this->post['quantity'];

		// email 
		// send email
		//$email = new Model_mandrill();
		
		// $email->html = '<p>Dear ' . $this->post['name'] . ',</p><br /><p>Thank you for completing the registration process with MyOnlineCopyright.com.</p><p>Here is your email and password: ' . $this->post['email'] . ' / ' . $this->post['password'] . '</p><p>You can start uploading your Original Creations and protect them with our service.<br />You can always check your status by visiting Your Account.</p><p>If you have questions, see our <a href="https://myonlinecopyright.com/frequently-asked-questions">FAQ</a> page or write us an email at <a href="mailto:contact@myonlinecopyright.com">contact@myonlinecopyright.com</a> (we will reply as soon as possible, promise).</p><br /><p>Thank you for choosing MyOnlineCopyright.com</p><p>MOC Staff</p>';
		// $email->subject = 'Thank you for registering with MyOnlineCopyright.com';
		// $email->from_email = $this->app->config->app->email->from_email;
		// $email->from_name = $this->app->config->app->email->from_name;
		
		// $email->addRecipient($account->email);
		
		//email details ( change accordingly)
		$to = $account->email;
		$from = $this->app->config->app->email->from_email;
		$subject = 'Thank you for registering with MyOnlineCopyright.com';
		$message = '<p>Dear ' . $this->post['name'] . ',</p><p>Thank you for completing the registration process with MyOnlineCopyright.com.<br />Here is your email and password: ' . $this->post['email'] . ' / ' . $this->post['password'] . '<br />You can start uploading your Original Creations and protect them with our service.<br />You can always check your status by visiting Your Account.<br />If you have questions, see our <a href="https://myonlinecopyright.com/frequently-asked-questions">FAQ</a> page or write us an email at <a href="mailto:contact@myonlinecopyright.com">contact@myonlinecopyright.com</a> (we will reply as soon as possible, promise).</p><p>Thank you for choosing MyOnlineCopyright.com<br />MOC Staff</p>';

		//select a random hash to send MIME content
		$hash = md5(time());

		//carriage return type
		$EOL = PHP_EOL;

		//main header (multipart mandatory)
		$headers  = "From: ".$from.$EOL;
		$headers .= "MIME-Version: 1.0".$EOL;
		$headers .= "Content-Type: multipart/mixed; boundary=\"".$hash."\"".$EOL.$EOL;
		$headers .= "Content-Transfer-Encoding: 7bit".$EOL;
		$headers .= "This is a MIME encoded message.".$EOL.$EOL;

		//message
		$headers .= "--".$hash.$EOL;
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$EOL;
		$headers .= "Content-Transfer-Encoding: 8bit".$EOL.$EOL;
		$headers .= $message.$EOL.$EOL;

		// create signin token
		$rememberMe = $this->post['rememberme'];

		// if "remember me" is not checked, set sign in to expire in 1 day
		if ( empty($rememberMe) )
		{
			$rememberMe = 24 * 60;
		}

		$expiration = new DateTime('now + ' . $rememberMe . ' minutes');



		$signin = R::dispense('signin');
		$signin->expire = $expiration->format('Y-m-d H:i:s');
		$account->ownSignin[] = $signin;


		try
		{
			// $email->send();
			mail($to, $subject, "", $headers);
			R::store($account);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		/*
		 * @todo welcome/validation email
		 * @todo 7 day email
		 */


		
		// set cookie for sign in
		$this->app->setCookie('signin', $account->id . $account->token . '-' . $signin->id . $signin->token, $rememberMe . ' minutes');



		// return account data
		Model_Flash::add('success', 'Welcome! Click the "add files" button to add your first file, or drag files anywhere on the page.');
		$this->app->redirect('/app');
	}



	public function update()
	{
		parent::requireSignedIn();
		
		
		
		$this->app->viewCss[] = '/bootstrap/css/bootstrap-datepicker.css';
		$this->app->viewCss[] = '/css/account-create.css';
		
		$this->app->viewJs[] = '/bootstrap/js/bootstrap-datepicker.js';
		$this->app->viewJs[] = '/js/account-create-ck.js';
		
		$this->app->viewData->editing = TRUE;
		$this->data = $this->app->account->getProperties();

		$this->app->viewData->stripe_subscription_quantity = $this->app->account->stripe_subscription_quantity;
		
		
		if ( !$this->request->isPost() ) $this->render($this->data, 'account/create');



		// validate input
		$validator = new Model_Validator( $this->post );

		if ( !empty($this->post['password']) )
		{
			$validator
				->minLength (4, 'Password must be longer than 4 characters')
				->maxLength (32, 'Password must be shorter than 32 characters')
				->validate ( 'password' );
		}

		$validator
			->required('You must supply an email address.')
			->email('You must supply a valid email address')
			->validate('email');
			
//		$validator
//			->required('You must supply a date of birth (required)')
//			->date('You must supply a valid date of birth (date)')
//			->validate('dateofbirth');

		$validator
			->required('You must supply a name (required)')
			->minLength (4, 'name must be longer than 4 characters')
			->maxLength (64, 'name must be shorter than 32 characters')
			->validate('name');
			
//		$validator
//			->required('You must supply a city of birth (required)')
//			->minLength (4, 'city of birth must be longer than 4 characters')
//			->maxLength (32, 'city of birth must be shorter than 32 characters')
//			->validate('cityofbirth');

		$validator
			->required('You must supply a country of birth (required)')
			->minLength (4, 'country of birth must be longer than 4 characters')
			->maxLength (32, 'country of birth must be shorter than 32 characters')
			->validate('countryofbirth');



		// check for errors
		if ($validator->hasErrors ())
		{
			$data = $this->post;
			$data['errors'] = $validator->getAllErrors();

			$this->render($data, 'account/create');
		}



		// create acount
		$this->app->account->email = $this->post['email'];
		if ( !empty($this->post['password']) )
		{
			$this->app->account->password = md5($this->post['password']);
		}
		$this->app->account->name = $this->post['name'];
		$this->app->account->dateofbirth = date('Y-m-d', strtotime($this->post['day'] . ' ' . $this->post['month'] . ' ' . $this->post['year']));
//		$account->cityofbirth = $this->post['cityofbirth'];
		$this->app->account->countryofbirth = $this->post['countryofbirth'];
		
		
		
		try
		{
			R::store($this->app->account);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		Model_Flash::add('success', 'Great, we\'ve updated your account');
		$this->app->redirect('/app');
	}



	public function delete($accountToken)
	{
		// get request
		$req = $this->app->request();
		$post = $req->params();

		try
		{
			$account = Model_Account::getFromToken($accountToken);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		$account->isactive = 0;
		$account->deleted = date('Y-m-d H:i:s');


		try
		{
			$id = R::store($account);
			$account->id = $id;
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}


		echo $this->json;
	}
	
	
	
	public function signout()
	{
		$this->app->deleteCookie('signin');
		
		Model_Flash::add('success', 'We\'ve signed you out. Sign back in if you like.');
		
		$this->app->redirect('/signin');
	}
	
	
	
	public function signin()
	{
		if ( !$this->request->isPost() ) $this->render();
		

		
		// validate input
		$validator = new Model_Validator( $this->post );

		$validator->required ( 'You must supply a password.' )
			->minLength (4, 'Password must be longer than 4 characters')
			->maxLength (32, 'Password must be shorter than 32 characters')
			->validate ( 'password' );

		$validator
			->required('You must supply an email address.')
			->email('You must supply a valid email address')
			->validate('email');
			

			
		// check for errors
		if ($validator->hasErrors ())
		{
			$data = $this->post;
			$data['errors'] = $validator->getAllErrors();

			$this->render($data);
		}
			


		// get our params
		$email = $this->post['email'];
		$password = $this->post['password'];
		$rememberMe = $this->post['rememberme'];


		// get acount
		try {
			$account = R::findOne(
				'account',
				' email = ? ',
				array($this->post['email'])
			);
		}
		catch (Exception $e)
		{
			$data = $this->post;
			$data['errors']['form'] = 'Whoops! We couldn\'t sign you in. Give it another try?';

			$this->render($data);
		}



		// if not found
		if ( $account->id == 0 )
		{
			$data = $this->post;
			$data['errors']['form'] = 'Whoops! We couldn\'t find your account. Give it another try?';

			$this->render($data);
		}



		// check password
		if ( md5($password) != $account->password )
		{
			$data = $this->post;
			$data['errors']['password'] = 'Whoops, right email address, but wrong password. Try again?';

			$this->render($data);
		}



		// create signin token
		$rememberMe = $this->post['rememberme'];
		
		// if "remember me" is not checked, set sign in to expire in 1 day
		if ( empty($rememberMe) )
		{
			$rememberMe = 24 * 60;
		}
		$expiration = new DateTime('now + ' . $rememberMe . ' minutes');



		$signin = R::dispense('signin');
		$signin->expire = $expiration->format('Y-m-d H:i:s');
		$account->ownSignin[] = $signin;



		try
		{
			R::store($account);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		// set cookie for sign in
		$this->app->setCookie('signin', $account->id . $account->token . '-' . $signin->id . $signin->token, $rememberMe . ' minutes');
		
		
		Model_Flash::add('success', 'Welcome back!');
		$this->app->redirect('/app');
	}



	public function password()
	{
		if ( !$this->request->isPost() ) $this->render();



		// validate input
		$validator = new Model_Validator( $this->post );
		
		$validator
			->required('You must supply an email address.')
			->email('You must supply a valid email address')
			->validate('email');



		// check for errors
		if ($validator->hasErrors ())
		{
			$data = $this->post;
			$data['errors'] = $validator->getAllErrors();

			$this->render($data);
		}



		try
		{
			$dupeAccount = R::findOne(
				'account',
				' email = ? ',
				array($this->post['email'])
			);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		if ($dupeAccount->id == 0)
		{
			$data = $this->post;
			$data['errors']['email'] = 'We couldn\'t find an account with that email address';
			$this->render($data);
		}


		$newPassword = substr(md5(rand()), 0, 8);

	// email 
		// send email
		// $email = new Model_mandrill();
		
		// $email->html = '<p>Your new password is: ' . $newPassword . '</p>';
		// $email->subject = 'Your new MyOnlineCopyright password';
		// $email->from_email = $this->app->config->app->email->from_email;
		// $email->from_name = $this->app->config->app->email->from_name;
		
		// $email->addRecipient($dupeAccount->email);

		//email details ( change accordingly)
		$to = $dupeAccount->email;
		$from = $this->app->config->app->email->from_email;
		$subject = 'Your new MyOnlineCopyright password';
		$message = '<p>Your new password is: ' . $newPassword . '</p>';

		//select a random hash to send MIME content
		$hash = md5(time());

		//carriage return type
		$EOL = PHP_EOL;

		//main header (multipart mandatory)
		$headers  = "From: ".$from.$EOL;
		$headers .= "MIME-Version: 1.0".$EOL;
		$headers .= "Content-Type: multipart/mixed; boundary=\"".$hash."\"".$EOL.$EOL;
		$headers .= "Content-Transfer-Encoding: 7bit".$EOL;
		$headers .= "This is a MIME encoded message.".$EOL.$EOL;

		//message
		$headers .= "--".$hash.$EOL;
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$EOL;
		$headers .= "Content-Transfer-Encoding: 8bit".$EOL.$EOL;
		$headers .= $message.$EOL.$EOL;

		try 
		{
			// $email->send();
			mail($to, $subject, "", $headers);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		$dupeAccount->password = md5($newPassword);



		try
		{
			R::store($dupeAccount);
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}



		$this->app->redirect('/signin');
	}
}


