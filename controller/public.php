<?php

class Controller_Public extends Controller_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->app->viewData->price = $this->app->config->stripe->plans->annual_subscription->price;
		
		$this->app->viewData->price1 = $this->app->config->stripe->plans->one->price;
		$this->app->viewData->price2 = $this->app->config->stripe->plans->two->price;
		$this->app->viewData->price3 = $this->app->config->stripe->plans->three->price;
		$this->app->viewData->price5 = $this->app->config->stripe->plans->five->price;
		$this->app->viewData->price10 = $this->app->config->stripe->plans->ten->price;
		$this->app->viewData->price25 = $this->app->config->stripe->plans->twentyfive->price;
	}



	public function temp()
	{
		$this->app->useLayout = FALSE;
		$this->render();
	}
	

	
	public function index()
	{
		$this->app->viewCss[] = '/css/homepage.css';
		
		$this->app->viewJs[] = '/js/homepage.js';
		
		
		$this->render();
	}
	
	
	
	public function contact()
	{
		if ( !$this->app->request()->isPost() ) $this->render();

		
		
		// validate input
		$validator = new Model_Validator( $this->post );

		
		
		// requires internet connection, so disable when not on the server
		if ( APPLICATION_ENV == 'production' )
		{
			$validator
				->required('You must supply an email address (required)')
				->email('You must supply a valid email address (email)')
				->validate('email');
		}

		$validator->required ( 'You must supply a message.' )
			->minLength (4, 'Title must be longer than 4 characters')
			->maxLength (1096, 'Title must be shorter than 1096 characters')
			->validate ( 'message' );



		// check for errors
		if ($validator->hasErrors ())
		{
			$data = $this->post;
			$data['errors'] = $validator->getAllErrors();

			$this->render($data);
		}
			
			

		// send email
		$email = new Model_mandrill();
		
		$email->text = $this->post['message'];
		$email->subject = 'Email from myOnlineCopyright contact form';
		$email->from_email = $this->post['email'];
		
		$email->addRecipient($this->app->config->app->email->from_email);



		try 
		{
			$email->send();
			$this->render(array('isSent' => TRUE));
		}
		catch (Exception $e)
		{
			$this->app->error($e);
		}
	}



	public function faq()
	{
		$this->render();
	}
	
	
	
	public function privacy()
	{
		$this->render();
	}
	
	
	
	public function terms()
	{
		$this->render();
	}
	
	
	
	public function test()
	{
		$this->render();
	}
	
	
	
	public function notFound()
	{
		$this->render();
	}

	
	
	public function error()
	{
		$this->render();
	}
	

	
}


