<?php

class Controller_Controller
{
	protected $app;
	protected $request;
	protected $json;
	protected $params = array();



	// https://github.com/silentworks/sharemyideas/blob/develop/app/application.php
	public function __construct(Slim\Slim $slim = null)
	{
		$this->app = ! empty ( $slim ) ? $slim : \Slim\Slim::getInstance ();
		
		$this->request = $this->app->request();

		$this->post = $this->request->params();

		// in case backbone is sending params in body
		// if ( $this->request->getMediaType() == 'application/json')
		// {
		// 	$bodyStr = $this->request->getBody();
		// 	$bodyJson = json_decode($bodyStr, TRUE);
		// 	$bodyArr = (array) $bodyJson;

		// 	$this->post = array_merge($this->post, $bodyArr);
		// }

		
		
		// set "view" variables
		$this->app->viewData = new stdClass;
		$this->app->useLayout = TRUE;
		$this->app->viewJs = array();
		$this->app->viewCss = array();


		
		// is user signed in?
		$this->app->viewData->signedIn = FALSE;
		$this->_authenticate();

		if ( $this->app->account->isadmin )
		{
			$this->app->viewData->isAdmin = TRUE;
		}
	}
	
	
	
	protected function _authenticate()
	{
		$token = $this->app->getCookie('signin');
		
		try 
		{
			$this->app->account = Model_Account::getFromToken($token);
		} 
		catch (Exception $e) 
		{
		}

		if ( $this->app->account->id > 0 )
		{
			$this->app->viewData->signedIn = TRUE;
			$this->app->viewData->account = $this->app->account;
			$this->app->viewData->accountToken = $token;
		}
	} 
	
	
	
	protected function requireSignedIn ()
	{
		if ( !$this->app->viewData->signedIn )
		{
			Model_Flash::add('error', 'Please sign in first');
			$this->app->redirect('/signin');
		}		
	}



	protected function requireAdmin ()
	{
		try
		{
			if ( !$this->app->viewData->isAdmin )
			{
				throw new Exception('You must be an admin');
			}
		}
		catch (Exception $e)
		{
			$this->app->error( $e );
		}
	}



	public function render($data = array(), $template = '')
	{
		$data = array_merge($data, (array) $this->app->viewData);

		if ( !empty($this->app->viewJs) )
		{
			foreach ( $this->app->viewJs as $script)
			{
				$js .= '<script src="' . $script . '"></script>';
			}
			
			$data['js'] = $js;
		}

		if ( !empty($this->app->viewCss) )
		{
			foreach ( $this->app->viewCss as $script)
			{
				$css .= '<link href="' . $script . '" rel="stylesheet">';
			}
			
			$data['css'] = $css;
		}
		
		if ( !empty($this->app->viewMeta) )
		{
			foreach ( $this->app->viewMeta as $name => $content)
			{
				$meta .= '<meta name="' . $name . '" content="' . $content . '">' . "\n";
			}
			
			$data['meta'] = $meta;
		}
		
		$data['useLayout'] = $this->app->useLayout;



		if ( empty($template) )
		{
			$trace=debug_backtrace();
			$caller = $trace[1];
			$function = $caller['function'];
			$class = strToLower($caller['class']);
			$classArr = explode('_', $class); 
			$template = $classArr[1] . '/' . $function;
		}

		// add to data for inserting with cms
		$data['template'] = $template;

		$this->app->render(strtolower($template) . '.php', $data);
		exit;
	}

	
	
}


