<?php

class Controller_File extends Controller_Controller
{
	
	
	
	public function download($downloadToken)
	{
		$token = Model_String::parseKey($downloadToken);

		// get account
		try
		{
			$download = R::findOne(
				'download',
				' `id` = ? AND `token` = ? AND `isactive` = 1',
				array($token->id, $token->token)
			);
		}
		catch (Exception $e)
		{
			$this->json->error('download not found', 404);
		}
		


		if ( strtotime($download->expire) < date('U') )
		{
			$this->json->error('download expired', 403);
		}
		
		
		
		// required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression'))
		{
			ini_set('zlib.output_compression', 'Off');
		}
		
		

		// http://davidwalsh.name/php-file-extension
		$file_extension = substr(strrchr($download->filename,'.'),1);;
		
		switch( $file_extension )
		{
			case "pdf": $ctype="application/pdf"; break;
			case "exe": $ctype="application/octet-stream"; break;
			case "zip": $ctype="application/zip"; break;
			case "doc": $ctype="application/msword"; break;
			case "xls": $ctype="application/vnd.ms-excel"; break;
			case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpg"; break;
			default: $ctype="application/force-download";
		}
		
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers
		header("Content-Type: $ctype");
		// change, added quotes to allow spaces in filenames, by Rajkumar Singh
		header("Content-Disposition: attachment; filename=\"" . $download->filename . "\";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($download->filepath));
		readfile("$download->filepath");
		exit();
	}
}