<?php

class Controller_Certificate extends Controller_Controller
{
        public function create($copyrightId)
        {
        parent::requireSignedIn();

        // get account
       try
       {
               $copyright = R::findOne(
                       'copyright',
                       ' id = ? AND isactive = 1',
                       array($copyrightId)
               );
       }
       catch (Exception $e)
       {
               $this->app->notFound($e);
       }



       if ( !$copyright ) $this->app->notFound();

       $token = $this->app->getCookie('signin');

       try
       {
               $this->app->account = Model_Account::getFromToken($token);
       } 
       catch (Exception $e) {
               $this->app->notFound();
       }

       if($copyrightData->account_id != $copy){
               $this->app->notFound();
       }

       
       require_once(APPLICATION_DIR . "/lib/dompdf-master/dompdf_config.inc.php");

      
      
      // get html
       $htmlToRender = file_get_contents($this->request->getUrl() . '/copyrights/' . $copyrightId . '/forpdf');

       // create pdf
       // https://code.google.com/p/dompdf/wiki/Usage
       $dompdf = new DOMPDF();
       $dompdf->load_html($htmlToRender);
       $dompdf->set_paper('A4', 'landscape');
       $dompdf->render();
       $pdfStr = $dompdf->output();

       // save, send, delete
      $output_file = $this->app->config->app->for_printing->path . '/copyright-' . $copyrightId . '.pdf';
      file_put_contents($output_file, $pdfStr);

      if ($output_file and file_exists($output_file)) {
         header('Content-Description: Your Certificate');
         header('Content-Type: application/pdf');
         header('Content-Disposition: attachment; filename="certificate.pdf"');
         header('Content-Transfer-Encoding: binary');
         header('Expires: 0');
         header('Cache-Control: must-revalidate');
         header('Pragma: public');
         header('Content-Length: ' . filesize($output_file));
         ob_clean();
         flush();
         if (readfile($output_file)) {
          unlink($output_file);
         }
      }

      
      ignore_user_abort(true);
      if (connection_aborted()) {
          unlink($output_file);
      }

        }
}
