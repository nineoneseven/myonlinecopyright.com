<?php

class Controller_System extends Controller_Controller
{
	public function stripe_webhooks ()
	{
		require_once(APPLICATION_DIR . 'lib/Stripe/Stripe.php');

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://manage.stripe.com/account
		// Stripe::setApiKey("sk_test_3ZcqpSfSCkPw8pOCwsX49bKy");
		Stripe::setApiKey($this->app->config->stripe->secret_key);

		// Retrieve the request's body and parse it as JSON
		$body = @file_get_contents('php://input');
		$event_json = json_decode($body);



		try 
		{    	
			$fd = fopen('../data/logs/stripe-webhook-' . date('y-m-d') . '.txt', 'a+');
			fwrite($fd, "\n\n--\n" . print_r($event_json, true));
			fclose($fd);
		}
		catch (Exception $e) {}


		// switch ($event_json->type)
		// {
			
		//     case 'charge.succeeded': 

		// 	$stripe_customerId = $event_json->data->object->customer
		// 	$customer = Stripe_Customer::retrieve($event_json->data->object->customer);
		// }
	}


	public function push_to_s3 ()
	{
		require_once(APPLICATION_DIR . 'lib/tpyo-amazon-s3-php-class-2061fa8/S3.php');



		// Instantiate the class
		$s3 = new S3(
			$this->app->config->aws->s3->key, 
			$this->app->config->aws->s3->secret
		);


		// get file that needs to be uploaded
		$copyright = R::findOne(
			'copyright',
        	' isstoredremotely = ? ',array('0')
        );

		// die if there are no files that need to be uploaded
		if ($copyright->id == 0) exit('no files to upload');



		$fileName = $copyright->account_id . '-' . $copyright->id;
		$filePath = $this->app->config->app->file_folder->path . $fileName;

		$bucketName = $this->app->config->aws->s3->bucket;



		// Put our file
		// http://undesigned.org.za/2007/10/22/amazon-s3-php-class/documentation
		if ( $s3->putObjectFile($filePath, $bucketName, baseName($filePath), S3::ACL_AUTHENTICATED_READ) ) 
		{
			echo "S3::putObjectFile(): File copied to {$bucketName}/".baseName($filePath).PHP_EOL;
		}



		// make sure it's stored
		$infoArr = $s3->getObjectInfo($bucketName, $fileName);



		// save the s3 time
		$copyright->s3storedtime = $infoArr['time'];
		$copyright->s3hash = $infoArr['hash'];

		// set as stored remotely for downloads
		$copyright->isstoredremotely = 1;

		try
		{
			R::store($copyright);
		}
		catch (Exception $e)
		{
			echo 'copyright record saved';
		}
	}



}


