<?php

class Controller_App extends Controller_Controller
{


	public function index()
	{
		parent::requireSignedIn();
		
		$this->app->viewCss[] = '/jQuery-File-Upload-master/css/jquery.fileupload-ui.css';
		$this->app->viewCss[] = '/css/app-index.css';
		
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/blueimp.github.com/tmpl.min.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/blueimp.github.com/load-image.min.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/blueimp.github.com/canvas-to-blob.min.js';
		// $this->app->viewJs[] = '/jQuery-File-Upload-master/blueimp.github.com/cdn/js/bootstrap.min.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/jquery.iframe-transport.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/jquery.fileupload.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/jquery.fileupload-fp.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/jquery.fileupload-ui.js';
		$this->app->viewJs[] = '/jQuery-File-Upload-master/js/main.js';		
		
		
		
		// $this->app->viewData->account = $this->app->account; // already set in controller_controller

		$this->app->viewData->files_diskspaceremaining = Model_Copyright::getFormattedFileSize(Model_Account::getDiskSpaceRemaining($this->app->account));
		$this->app->viewData->account->files_diskspaceused = Model_Copyright::getFormattedFileSize($this->app->viewData->account->files_diskspaceused);
		
		
		
		$this->render();
	}
}

